Instalación

Ubicarse en la carpeta saas/proyectoWWW

1)Instalar los requerimientos pip3 install -r requirements.txt

2)Crear una base de datos para el proyecto y actualizar la configuración de BD en secrets.json

3)Sincronizar la base de datos: python manage.py migrate

4)Ejecutar en la base de datos el archivo  proyectoWWW/_datos_iniciales/1_tenant_publico.sql

Ubicarse en la carpeta sass/pagoswww/pagos

1)Actualizar la configuración de BD en pagos/settings.py

2)Sincronizar la base de datos: python manage.py migrate

Ejecución en desarrollo

1) En una terminal iniciar servidor de correo de pruebas MailHog  (https://github.com/mailhog/MailHog/releases)

Nota: Al iniciar el servidor se puede ver en ejecucion en localhost:8025

2) En una terminal ubicarse en la carpeta saas/proyectoWWW

3) Ejecutar: python manage.py runserver 8000

4) Ingresar a la aplicacion en el navegador con la URL localhost:8000

5) Ingresar al sistema con el usuario "admin" y la contraseña "admin123"

6) En otra terminal ubicarse en la carpeta saas/pagoswww/pagos

7) Ejecutar: python manage.py runserver 8001 (importante el puerto dado que la configuración de la aplicación principal 
buscará el webservice de pago en localhost:8001 que se encuentra configurado en settings

8) Ingresar a la aplicacion en el navegador con la URL localhost:8001




Email
Tutorial para crear ambiente con python3 usando virtualenvwrapper

0)Se parte del supuesto que ya se tiene instalado virtualenvwrapper

1)Ejecutar: mkproject nombre <-- Esto crea un projecto y ambiente con el "nombre" que se de

2)Ejecutar: deactivate <-- Con esto salimos del ambiente

3)Ejecutar: rmvirtualenv nombreEnv  <-- Eliminaremos el ambiente que creamos con mkproject

4)Ejecutar: lsvirtualenv <-- lista los ambientes existente, asegurarse de que ya no aparezca el ambiente que eliminamos

5)Ejecutar: mkvirtualenv --python=/usr/bin/python3 nombreENV <-- Creamos un ambiente con python3

6)Ejecutar: lsvirtualenv <-- Nos aseguramos que el ambiente que creamos aparezca

7)Ejecutar: setvirtualenvproject <-- Asegurese que esta activado el ambiente que creo anteriormente y que se encuentra situado en la carpeta a la que le quiere asignar el ambiente

8)Ejecutar: python: Si el interprete muestra python3.5, entonces todo quedo perfecto.