//script que trasfiere los datos de la educación a eliminar al modal de confirmacion
$(".table").on('click','.desactivar', function (e) {
    var url = $(this).attr('data-url');
    var actividad = $(this).data('listado-nombre');
    var ubicacion = $(this).data('listado-ubicacion');
    var fecha_inicio = $(this).data('listado-fecha_inicio');
    var evento = $(this).data('listado-evento');
    var estado =  $(this).data('listado-estado');

    $(".listado-nombre").text(evento);
    $(".listado-ubicacion").text(ubicacion);
    $(".listado-fecha_inicio").text(fecha_inicio);
    $(".listado-evento").text(evento);
    $(".listado-estado").text(estado);

    $("#confirmar-desactivar").attr('href', url);
});

$(".table").on('click','.activar', function (e) {
    var url = $(this).attr('data-url');
    var actividad = $(this).data('listado-nombre');
    var ubicacion = $(this).data('listado-ubicacion');
    var fecha_inicio = $(this).data('listado-fecha_inicio');
    var evento = $(this).data('listado-evento');
    var estado =  $(this).data('listado-estado');

    $(".listado-nombre").text(evento);
    $(".listado-ubicacion").text(ubicacion);
    $(".listado-fecha_inicio").text(fecha_inicio);
    $(".listado-evento").text(evento);
    $(".listado-estado").text(estado);

    $("#confirmar-activar").attr('href', url);
});

$(".table").on('click','.eliminar', function (e) {
    var url = $(this).attr('data-url');
    var tipoLista = $(this).data('listado-tipoLista');
    var actividad_nombre = $(this).data('listado-nombre');
    var actividad_fecha_inicio = $(this).data('listado-fecha_inicio');
    var actividad_horario = $(this).data('listado-horario');
    var actividad_ubicacion = $(this).data('listado-ubicacion');
    var actividad_evento = $(this).data('listado-evento');

    $('.listado-nombre').text(actividad_nombre)
    $('.listado-fecha_inicio').text(actividad_fecha_inicio)
    $('.listado-horario').text(actividad_horario)
    $('.listado-ubicacion').text(actividad_ubicacion)
    $('.listado-evento').text(actividad_evento)
    $("#confirmar-eliminar").attr('href', url);
});
