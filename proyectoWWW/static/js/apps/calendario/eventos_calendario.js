function actualizarCalendario(panel_calendario,selector,url_ajax) {
    $('#enviar-evento').on('click',function () {
        visualizar_actividades_evento(panel_calendario,selector,url_ajax)
    });
}


function visualizar_actividades_evento(panel_calendario, selector, url_ajax) {
    var id_evento = $("#" + selector + " option:selected").val();

    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            "pk_evento": id_evento
        },
        success: function (datos) {
            //$('#contenedor-grafico').empty();
            //$('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var actividades = datos.eventos;
            for (actividad in actividades) {

                var programacion = {
                    title: actividades[actividad].titulo,
                    start: actividades[actividad].fechaInicio,
                    end: actividades[actividad].fechaFin,
                    color: actividades[actividad].color,
                }
                $('.calendar').fullCalendar('renderEvent', programacion, 'stick');
            }


        }
    });
}
