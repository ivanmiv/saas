//script que trasfiere los datos de la educación a eliminar al modal de confirmacion
$(".table").on('click','.eliminar', function (e) {
    var url = $(this).attr('data-url');
    var noticia = $(this).data('listado-titulo');
    var descripcion = $(this).data('listado-descripcion');
    var fecha = $(this).data('listado-fecha');

    $(".listado-titulo").text(noticia);
    $(".listado-descripcion").text(descripcion);
    $(".listado-fecha").text(fecha);

    $("#confirmar-eliminar").attr('href', url);
});
