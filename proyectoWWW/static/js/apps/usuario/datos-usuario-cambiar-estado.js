//script que trasfiere los datos del usuario a modificar al modal de confirmación
$(".table").on('click','.modificar', function (e) {
    var url = $(this).attr('data-url');
    var nombre = $(this).data('listado-nombre');
    var apellido = $(this).data('listado-apellido');
    var estado = $(this).data('listado-estado');
    var estado_retornar="no leyo";
    
    $(".listado-nombre").text(nombre);
    $(".listado-apellido").text(apellido);

    if(estado=='True')
    {
        estado_retornar="Activo"
    }else
    {
        estado_retornar="Inactivo"
    }

    $(".listado-estado").text(estado_retornar);

    $("#confirmar-modificar").attr('href', url);
});
