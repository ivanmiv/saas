var map;

function generarMapa() {

    ubicacion = {lat: latitud, lng: longitud};

    map = new google.maps.Map(document.getElementById('mapa_institucion'), {
        center: ubicacion,
        zoom: 15
    });

    var marker = new google.maps.Marker({
        position: ubicacion,
        map: map,
        title: "{{ evento.address }}",
    });

}
