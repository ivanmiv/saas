//script que trasfiere los datos del evento a modificar al modal de confirmación
$(".table").on('click','.activar-desactivar', function (e) {
    var url = $(this).attr('data-url');
    var evento = $(this).data('listado-nombre');
    var descripcion = $(this).data('listado-descripcion');
    var cantidad_participantes = $(this).data('listado-cantidad-participantes');
    var estado =  $(this).data('listado-estado');

    $(".listado-nombre").text(evento);
    $(".listado-descripcion").text(descripcion);
    $(".listado-cantidad-participantes").text(cantidad_participantes);
    $(".listado-estado").text(estado);

    $("#confirmar-activar-desactivar").attr('href', url);
});
