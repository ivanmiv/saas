//script que trasfiere los datos del usuario a modificar al modal de aceptacion o rechazo de los participantes
$(".table").on('click','.registrar', function (e) {
    var url = $(this).attr('data-url');
    var nombre = $(this).data('listado-nombre');
    var apellido = $(this).data('listado-apellido');
    var cedula = $(this).data('listado-cedula');
    var evento = $(this).data('listado-evento');


    $(".listado-nombre").text(nombre);
    $(".listado-apellido").text(apellido);
    $(".listado-cedula").text(cedula);
    $(".listado-evento").text(evento);

    $("#confirmar-aceptar").attr('href', url);
});
