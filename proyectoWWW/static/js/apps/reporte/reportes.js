// Para usarse cuando se necesite
// var fecha_inicio = $("input[name=start]").val();
// var fecha_fin =  $("input[name=end]").val();



function generar_reporte_barras(url_ajax, panel_grafico, tipo_grafico, l_barra, titulo_char) {

    var  tipo = tipo_grafico;

    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        success: function (data) {
            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: l_barra,
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = data.ejeY;
            var titulo_ejeX = data.ejeX;

            var barChart = new Chart(contexto, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: titulo_char
                    },
                    legend: {
                        display: false
                    },
                    scales:{
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }, ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }, ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        }
    });
}

function generar_reporte_barras_asistencias(url_ajax, panel_grafico, selector_fecha,tipo_grafico) {

    var  tipo = tipo_grafico;
    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        success: function (data) {
            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: "Asistencia",
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = data.ejeY;
            var titulo_ejeX = data.ejeX;

            var barChart = new Chart(contexto, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Top 5 de asistencias por evento'
                    },
                    legend: {
                        display: false
                    },
                    scales:{
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }, ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }, ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        }
    });
}

function generar_reporte_barras_cantidad_mes(url_ajax, panel_grafico, tipo_grafico,l_barra,titulo_char,selector) {

    var anho = $("#" + selector + " option:selected").val();

    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            'anho': anho
        },
        success: function (data) {
            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: l_barra,
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = data.ejeY;
            var titulo_ejeX = data.ejeX;

            var barChart = new Chart(contexto, {
                type: tipo_grafico,
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: titulo_char
                    },
                    legend: {
                        display: false
                    },
                    scales:{
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }, ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }, ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        }
    });
}


function generar_reporte_ingresos(boton_actualizar, url_ajax, panel_grafico,label, titulo) {
    var tipo_grafico = 'bar';
    $(boton_actualizar).on('click', function () {
        generar_reporte_barras(url_ajax, panel_grafico, tipo_grafico, label, titulo);
    });
    //Para que se ejecute la función al randerizarse la pagina
    generar_reporte_barras(url_ajax, panel_grafico, tipo_grafico, label, titulo);

    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        generar_reporte_barras(url_ajax, panel_grafico, tipo_grafico, label, titulo);

    });
}


    function generar_reporte_asistencias(boton_actualizar, url_ajax, panel_grafico) {
    var tipo_grafico = 'bar';
    $(boton_actualizar).on('click', function () {
         generar_reporte_barras_asistencias(url_ajax, panel_grafico, tipo_grafico);
    });
    //Para que se ejecute la función al randerizarse la pagina
    generar_reporte_barras_asistencias(url_ajax, panel_grafico, tipo_grafico);

    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        generar_reporte_barras_asistencias(url_ajax, panel_grafico, tipo_grafico);

    });
}

function generar_reporte_cantidad_mes(selector,boton_actualizar, url_ajax, panel_grafico,label, titulo) {

    var tipo_grafico = 'bar';
    $(boton_actualizar).on('click', function () {
        generar_reporte_barras_cantidad_mes(url_ajax, panel_grafico, tipo_grafico, label, titulo,selector);
    });
    //Para que se ejecute la función al randerizarse la pagina
    generar_reporte_barras_cantidad_mes(url_ajax, panel_grafico, tipo_grafico, label, titulo,selector);

    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        generar_reporte_barras_cantidad_mes(url_ajax, panel_grafico, tipo_grafico, label, titulo,selector);

    });
}


