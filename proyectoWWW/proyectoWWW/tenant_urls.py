from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r"^actividad/", include("apps.Actividad.urls")),
    url(r"^evento/", include("apps.Evento.urls")),
    url(r"^tema/", include("apps.tema.urls")),
    url(r"^usuario/", include("apps.Usuario.urls")),
    url(r"^noticia/", include("apps.Noticia.urls")),
    url(r"^calendario/", include("apps.Calendario.urls")),
    url(r"^participante/", include("apps.Participante.urls")),
    url(r"^reportes/", include("apps.Reporte.urls")),
    url(r"^", include("apps.vista_publica.urls")),
    url(r"^select2/", include("django_select2.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
