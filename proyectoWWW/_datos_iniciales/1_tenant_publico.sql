INSERT INTO "Usuario_usuario" ("date_joined","first_name","last_name","username","email","is_superuser","is_active","is_staff","password","cedula","edad","cargo","sexo","imagen") VALUES
        ('5000-12-31','','','admin','root@gmail.com',true,true,true ,'pbkdf2_sha256$36000$UhDStYy6ygQk$kU55LwrvMHjDcpILTHreRzEzKCupRPKjZ7zxa5fk4C8=',000100,01,'Administrador','M','');

INSERT INTO "empresa_modulo" ("nombre") VALUES ('Actividades');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Calendario');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Eventos');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Noticias');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Participantes');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Reportes');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Temas');
INSERT INTO "empresa_modulo" ("nombre") VALUES ('Usuarios');

INSERT INTO "empresa_plan" ("nombre", "precio", "maximo_usuarios") VALUES
        ('Gratuito',0, 5);
INSERT INTO "empresa_plan" ("nombre", "precio", "maximo_usuarios") VALUES
        ('Estandar',25, 100);
INSERT INTO "empresa_plan" ("nombre", "precio", "maximo_usuarios") VALUES
        ('Empresarial',199, 1000);

INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 1);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 2);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 3);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 4);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 5);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 7);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (1, 8);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 1);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 2);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 3);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 4);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 5);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 6);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 7);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (2, 8);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 1);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 2);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 3);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 4);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 5);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 6);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 7);
INSERT INTO "empresa_plan_modulos_disponibles" (plan_id, modulo_id) VALUES (3, 8);


INSERT INTO "empresa_empresa" ("schema_name", "nombre", "plan_id", "tema", "color","vigencia","cliente_id") VALUES
        ('public', 'public',1,'a','mint','2099-12-31',1);

INSERT INTO "empresa_dominio" ("domain", "is_primary", "tenant_id") VALUES
        ('localhost', true, 1);