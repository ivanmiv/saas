from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from .forms import *

from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)


class RegistrarTema(LoginRequiredMixin, CreateView):
    model = Tema
    form_class = TemaForm
    template_name = "tema/templateTema.html"
    success_url = reverse_lazy("tema_registro_tema")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Temas")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrarTema, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistrarTema, self).get_context_data(**kwargs)
        context["titulo"] = "Formulario de registro de temas"
        context["temas"] = Tema.objects.all()
        context["accion"] = "Registro"
        return context

    def form_valid(self, form):
        messages.success(self.request, "Se ha registrado exitosamente el tema")
        return super(RegistrarTema, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request, "Error, no se registró el tema, por favor verificar los datos"
        )
        return super(RegistrarTema, self).form_invalid(form)


class ModificarTema(LoginRequiredMixin, UpdateView):
    model = Tema
    form_class = TemaForm
    template_name = "tema/templateTema.html"
    success_url = reverse_lazy("tema_registro_tema")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Temas")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ModificarTema, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModificarTema, self).get_context_data(**kwargs)
        context["titulo"] = "Formulario de registro de temas"
        context["temas"] = Tema.objects.all()
        context["accion"] = "Modificación"
        return context

    def get(self, *args, **kwargs):
        try:
            Tema.objects.get(pk=self.kwargs["pk"])
        except Tema.DoesNotExist:
            messages.error(self.request, "Error, el tema a modificar no existe")
            return redirect(reverse_lazy("tema_registro_tema"))
        return super(ModificarTema, self).get(*args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, "Se ha modificado exitosamente el tema")
        return super(ModificarTema, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request, "Error, no se modificó el tema, por favor verificar los datos"
        )
        return super(ModificarTema, self).form_invalid(form)


@verificar_modulo_incluido("Temas")
@verificar_cargo(cargos_permitidos=["Operador"])
@vigencia_disponible_empresa
def eliminarTema(request, id_tema=None):
    try:
        tema = Tema.objects.get(pk=id_tema)
        tema.delete()
        messages.success(request, "Se ha eliminado exitosamente el tema")
        return redirect("tema_registro_tema")
    except Tema.DoesNotExist:
        messages.error(request, "Error, el tema a eliminar no existe")
        return redirect("tema_registro_tema")
