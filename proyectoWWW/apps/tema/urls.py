from django.conf.urls import url
from .views import *

urlpatterns = [
    # Vistas de los temas
    url(r"^registro$", RegistrarTema.as_view(), name="tema_registro_tema"),
    url(
        r"^modificar/(?P<pk>\d+)$", ModificarTema.as_view(), name="tema_modificar_tema"
    ),
    url(r"^eliminar/(?P<id_tema>\d+)$", eliminarTema, name="tema_eliminar_tema"),
]
