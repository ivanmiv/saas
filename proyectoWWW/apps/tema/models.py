from django.db import models
from simple_history.models import HistoricalRecords


class Registro(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    historia = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True


class Tema(models.Model):
    nombre = models.CharField(unique=True, max_length=30, verbose_name="nombre")

    def __str__(self):
        return self.nombre
