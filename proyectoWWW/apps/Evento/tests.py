from django.urls import reverse_lazy

from django_tenants.test.cases import TenantTestCase
from django_tenants.test.client import TenantClient
from django.utils import timezone
from datetime import date

from proyectoWWW.utilidades import obtener_mensajes_respuesta
from .models import *
from apps.empresa.models import Plan
from apps.Usuario.models import Usuario
from apps.tema.models import Tema


class RegistrarEventoTestCase(TenantTestCase):
    def setup_tenant(self, tenant):
        from django.utils import timezone

        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return "localhost"

    @staticmethod
    def get_test_schema_name():
        return "tester"

    def setUp(self):
        super(RegistrarEventoTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user(
            "operador",
            "root@gmail.com",
            password,
            cedula=123,
            edad=30,
            telefono=5555555,
        )
        user.set_password(password)
        user.first_name = "cliente"
        user.is_superuser = False
        user.is_staff = False
        user.cargo = "Operador"
        user.save()

        password = "gerente123"
        gerente = Usuario.objects.create_user(
            "gerente", "root@gmail.com", password, cedula=456, edad=30, telefono=5555555
        )
        gerente.set_password(password)
        gerente.first_name = "gerente"
        gerente.is_superuser = False
        gerente.is_staff = False
        gerente.cargo = "Gerente"
        gerente.save()

        tema = Tema.objects.create(nombre="Musica")
        tema.save()

        evento = Evento.objects.create(
            nombre="Evento prueba",
            ubicacion="Univalle",
            precio=1000,
            fecha_inicio=date(2018, 7, 4),
            fecha_fin=date(2018, 7, 23),
            numero_participantes=20,
            address="Univalle",
            geolocation="45,45",
        )
        evento.temas.add(tema)
        evento.save()

    def test_registro_evento(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Univalle",
                "precio": 1000,
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("El evento se ha guardado satisfactoriamente", mensaje)

    def test_registro_evento_sin_login(self):
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Evento",
                "ubicacion": "Univalle",
                "precio": 1000,
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensaje)

    def test_registro_evento_sin_nombre(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "",
                "ubicacion": "Univalle",
                "precio": 1000,
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_ubicacion(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "",
                "precio": 1000,
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_precio(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Coliseo del pueblo",
                "precio": "",
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_fecha_inicio(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Coliseo del pueblo",
                "precio": "",
                "fecha_inicio": "",
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_fecha_fin(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Coliseo del pueblo",
                "precio": "",
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": "",
                "numero_participantes": 50,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_participantes(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Coliseo del pueblo",
                "precio": "",
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": "",
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_registro_evento_sin_temas(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_registro_evento"),
            {
                "nombre": "Mundial de Salsa Cali 2018",
                "ubicacion": "Coliseo del pueblo",
                "precio": "",
                "fecha_inicio": date(2018, 7, 4),
                "fecha_fin": date(2018, 7, 23),
                "numero_participantes": 50,
                "temas": "",
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró el evento, por favor verificar los datos", mensaje
        )

    def test_activar_evento(self):
        self.cliente.login(username="gerente", password="gerente123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_activar_desactivar_evento", kwargs={"id_evento": 1})
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Estado del evento modificado exitosamente", mensaje)

    def test_detalle_evento(self):
        respuesta = self.cliente.post(
            reverse_lazy("evento_detalle_evento", kwargs={"id_evento": 1})
        )
        self.assertEquals(respuesta.status_code, 200)


class ModificarEventoTestCase(TenantTestCase):
    def setup_tenant(self, tenant):
        from django.utils import timezone

        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return "localhost"

    @staticmethod
    def get_test_schema_name():
        return "tester"

    def setUp(self):
        super(ModificarEventoTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user(
            "operador",
            "root@gmail.com",
            password,
            cedula=123,
            edad=30,
            telefono=5555555,
        )
        user.set_password(password)
        user.first_name = "cliente"
        user.is_superuser = False
        user.is_staff = False
        user.cargo = "Operador"
        user.save()

        password = "gerente123"
        gerente = Usuario.objects.create_user(
            "gerente", "root@gmail.com", password, cedula=456, edad=30, telefono=5555555
        )
        gerente.set_password(password)
        gerente.first_name = "gerente"
        gerente.is_superuser = False
        gerente.is_staff = False
        gerente.cargo = "Gerente"
        gerente.save()

        tema = Tema.objects.create(nombre="Cultura")
        tema.save()

        evento = Evento.objects.create(
            nombre="Evento prueba",
            ubicacion="Univalle",
            precio=1000,
            fecha_inicio=date(2018, 7, 4),
            fecha_fin=date(2018, 7, 23),
            numero_participantes=20,
            address="Univalle",
            geolocation="45,45",
        )
        evento.temas.add(tema)
        evento.save()

    def test_modificar_evento(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Exposicion de pintura ",
                "ubicacion": "USC",
                "precio": 5000,
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("El evento se ha actualizado satisfactoriamente", mensaje)

    def test_modificar_evento_sin_login(self):
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Evento",
                "ubicacion": "Univalle",
                "precio": 1000,
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensaje)

    def test_modificar_evento_sin_nombre(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "",
                "ubicacion": "Museo de la tertulia",
                "precio": 12000,
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se actualizó el evento, por favor verificar los datos", mensaje
        )

    def test_modificar_evento_sin_ubicacion(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Exposicion de pintura ",
                "ubicacion": "",
                "precio": 12000,
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se actualizó el evento, por favor verificar los datos", mensaje
        )

    def test_modificar_evento_sin_precio(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Exposicion de pintura ",
                "ubicacion": "Museo de la tertulia",
                "precio": "",
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se actualizó el evento, por favor verificar los datos", mensaje
        )

    def test_modificar_evento_sin_fecha_inicio(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Exposicion de pintura ",
                "ubicacion": "Museo de la tertulia",
                "precio": 12000,
                "fecha_inicio": "",
                "fecha_fin": date(2018, 9, 22),
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se actualizó el evento, por favor verificar los datos", mensaje
        )

    def test_modificar_evento_sin_fecha_fin(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("evento_modificar_evento", kwargs={"pk": 1}),
            {
                "nombre": "Exposicion de pintura ",
                "ubicacion": "Museo de la tertulia",
                "precio": 12000,
                "fecha_inicio": date(2018, 9, 18),
                "fecha_fin": "",
                "numero_participantes": 100,
                "temas": [1],
                "address": "Coliseo del pueblo",
                "geolocation": "45,45",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se actualizó el evento, por favor verificar los datos", mensaje
        )
