from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView

from .forms import RegistroEventoForm, CedulaForm
from .models import Evento
from apps.Actividad.models import Actividad
from apps.Participante.models import ParticipantesEventos
from apps.Usuario.models import Usuario

from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)


@verificar_modulo_incluido("Eventos")
@verificar_cargo(cargos_permitidos=["Gerente"])
@vigencia_disponible_empresa
def activar_desactivar_evento(request, id_evento):
    try:
        evento = Evento.objects.get(id=id_evento)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento a modificar no existe")
        return redirect("inicio")

    if evento.estado == "INACTIVO":
        evento.estado = "ACTIVO"
    else:
        evento.estado = "INACTIVO"
    evento.save()
    messages.success(request, "Estado del evento modificado exitosamente")
    return redirect("evento_listado_evento")


@verificar_modulo_incluido("Eventos")
@verificar_cargo(cargos_permitidos=["Operador"])
@vigencia_disponible_empresa
def confirmar_participante(request, id_participante, id_evento):
    try:
        evento = Evento.objects.get(pk=id_evento)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento no existe")
        return redirect("inicio")

    try:
        participante = Usuario.objects.get(id=id_participante)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el participante no existe")
        return redirect("inicio")

    try:
        participanteAEvento = ParticipantesEventos.objects.get(
            evento=evento, participante=participante
        )
    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento a modificar no existe")
        return redirect("inicio")

    if (
        evento.interoperabilidad_pagos(participante.cedula, request.tenant.schema_name)
        == 1
    ):
        participanteAEvento.estado_participante = "INSCRITO"
        participanteAEvento.save()
        evento.enviar_correo_aceptacion(request, participante)
        messages.success(request, "Inscripción realizada con éxito")
    elif (
        evento.interoperabilidad_pagos(participante.cedula, request.tenant.schema_name)
        == 0
    ):
        messages.error(request, "Error, el participante no ha realizado el pago")
    else:
        messages.error(
            request,
            "Ha ocurrido un error al realizar la solicitud al servicio web de pagos",
        )
    return redirect("evento_listado_evento")


@verificar_modulo_incluido("Eventos")
@vigencia_disponible_empresa
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def dashboard_evento(request, id_evento):
    from django.db.models import Sum

    try:
        evento = Evento.objects.get(id=id_evento)
        actividades = Actividad.objects.filter(evento=evento, estado="ACTIVO")
        participantes_preinscritos = ParticipantesEventos.objects.filter(
            evento=evento, estado_participante="PREINSCRITO"
        ).count()
        participantes_inscritos = ParticipantesEventos.objects.filter(
            evento=evento, estado_participante="INSCRITO"
        )
        ingresos = participantes_inscritos.aggregate(ingresos=Sum("evento__precio"))
        # ingresos = 0
    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento a visualizar no existe")
        return redirect("inicio")

    contexto = {
        "evento": evento,
        "actividades": actividades,
        "titulo": "Dashboard evento",
        "preinscritos": participantes_preinscritos,
        "GOOGLE_MAPS_KEY": settings.GOOGLE_MAPS_API_KEY,
        "inscritos": participantes_inscritos.count(),
        "ingresos": ingresos["ingresos"],
    }
    return render(request, "Evento/dashboardEvento.html", contexto)


@verificar_modulo_incluido("Eventos")
@vigencia_disponible_empresa
def detalle_evento(request, id_evento):
    import datetime

    try:
        evento = Evento.objects.get(id=id_evento)
        form = CedulaForm
        try:
            actividades = Actividad.objects.filter(evento=evento, estado="ACTIVO")
            request.session["id_evento"] = evento.pk

        except ObjectDoesNotExist:
            messages.error(request, "Error,  modificar no existe")
            return redirect("inicio")
        try:
            asistencias = ParticipantesEventos.objects.filter(evento=evento)
            num_participantes = asistencias.count()
            # request.session['id_evento'] = evento.pk
        except ObjectDoesNotExist:
            messages.error(request, "Error,  modificar no existe")
            return redirect("inicio")

    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento a visualizar no existe")
        return redirect("inicio")

    fecha_actual = datetime.date.today()
    usuario = request.user
    precio = evento.precio
    if hasattr(usuario, "fecha_fin_membresia"):
        if usuario.fecha_fin_membresia:
            if fecha_actual <= usuario.fecha_fin_membresia:
                precio = evento.precio - evento.precio * 0.3
    contexto = {
        "evento": evento,
        "actividades": actividades,
        "titulo": "Detalle evento",
        "num_participantes": num_participantes,
        "form": form,
        "GOOGLE_MAPS_KEY": settings.GOOGLE_MAPS_API_KEY,
        "precio": precio,
    }
    return render(request, "Evento/vistaDetalladaEvento.html", contexto)


class RegistroEvento(CreateView):
    form_class = RegistroEventoForm
    template_name = "Evento/templateEvento.html"
    success_url = reverse_lazy("evento_listado_evento")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Eventos")
    @vigencia_disponible_empresa
    @verificar_cargo(cargos_permitidos=["Operador"])
    def dispatch(self, request, *args, **kwargs):
        return super(RegistroEvento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistroEvento, self).get_context_data(**kwargs)
        context["titulo"] = "Registro de evento"
        context["tipo"] = "Evento"
        context["accion"] = "Registro"
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(self.request, "El evento se ha guardado satisfactoriamente")
        return super(RegistroEvento, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se registró el evento, por favor verificar los datos",
        )
        return super(RegistroEvento, self).form_invalid(form)


class ModificarEvento(UpdateView):
    model = Evento
    form_class = RegistroEventoForm
    template_name = "Evento/templateEvento.html"
    success_url = reverse_lazy("evento_listado_evento")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Eventos")
    @vigencia_disponible_empresa
    @verificar_cargo(cargos_permitidos=["Operador"])
    def dispatch(self, request, *args, **kwargs):
        return super(ModificarEvento, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModificarEvento, self).get_context_data(**kwargs)
        context["titulo"] = "Modificación de evento"
        context["tipo"] = "Evento"
        context["accion"] = "Modificación"
        return context

    def get(self, *args, **kwargs):
        try:
            Evento.objects.get(pk=self.kwargs["pk"])
        except ObjectDoesNotExist:
            messages.error(self.request, "Error, el evento a modificar no existe")
            return redirect(reverse_lazy("evento_listado_evento"))
        return super(ModificarEvento, self).get(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(self.request, "El evento se ha actualizado satisfactoriamente")
        return super(ModificarEvento, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se actualizó el evento, por favor verificar los datos",
        )
        return super(ModificarEvento, self).form_invalid(form)


class ListarEventos(ListView):
    model = Evento
    template_name = "Evento/listado.html"
    context_object_name = "lista_elementos"
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Eventos")
    @vigencia_disponible_empresa
    @verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
    def dispatch(self, request, *args, **kwargs):
        return super(ListarEventos, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarEventos, self).get_context_data(**kwargs)
        context["titulo"] = "Listado de eventos"
        context["lista_encabezados"] = [
            "Nombre",
            "Total de participantes",
            "Fecha",
            "Estado",
            "Acciones",
        ]
        context["tipoLista"] = "Evento"
        context["accion"] = "Listado"

        return context


@verificar_modulo_incluido("Eventos")
@vigencia_disponible_empresa
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def ListarParticipantesEvento(request, id_evento):
    try:
        evento = Evento.objects.get(pk=id_evento)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el evento no existe")
        return redirect("inicio")

    listado_participantes = []
    try:
        participantes_evento = ParticipantesEventos.objects.filter(evento=evento)
        for participante_evento in participantes_evento:
            listado_participantes.append(participante_evento.participante)
    except ObjectDoesNotExist:
        messages.error(request, "Error, no existen participantes en el evento")
        return redirect("inicio")

    contexto = {
        "asistencias": participantes_evento,
        "evento": evento,
        "lista_encabezados": ["No. identificación", "Nombre", "Apellidos", "Estado"],
        "titulo": "Participantes del evento %s" % (evento.nombre),
        "tipoLista": "Evento",
        "accion": "Listar participantes evento",
    }

    return render(request, "Evento/listadoParticipantesEvento.html", contexto)
