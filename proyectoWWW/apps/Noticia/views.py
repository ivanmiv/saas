from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
    RedirectView,
)

from apps.Noticia.models import *
from apps.Noticia.forms import *
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)


class RegistroNoticia(CreateView):
    form_class = NoticiaForm
    template_name = "noticia_form.html"
    success_url = reverse_lazy("noticia_registro")
    # login_url = reverse_lazy('login')

    @verificar_modulo_incluido("Noticias")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(RegistroNoticia, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistroNoticia, self).get_context_data(**kwargs)
        context["titulo"] = "Registro de Noticia"
        context["tipo"] = "Noticia"
        context["accion"] = "Registro"
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(self.request, "La noticia se ha guardado satisfactoriamente")
        return super(RegistroNoticia, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se registró la noticia, por favor verificar los datos",
        )
        return super(RegistroNoticia, self).form_invalid(form)


class ModificarNoticia(UpdateView):
    model = Noticia
    form_class = NoticiaForm
    template_name = "noticia_form.html"
    success_url = reverse_lazy("listado_noticias")
    # login_url = reverse_lazy('login')

    @verificar_modulo_incluido("Noticias")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ModificarNoticia, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModificarNoticia, self).get_context_data(**kwargs)
        context["titulo"] = "Modificación de noticia"
        context["tipo"] = "Noticia"
        context["accion"] = "Modificación"
        return context

    def get(self, *args, **kwargs):
        try:
            Noticia.objects.get(pk=self.kwargs["pk"])
        except ObjectDoesNotExist:
            messages.error(self.request, "Error, la noticia a modificar no existe")
            return redirect(reverse_lazy("listado_noticias"))
        return super(ModificarNoticia, self).get(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(
            self.request, "La noticia se ha actualizado satisfactoriamente"
        )
        return super(ModificarNoticia, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se actualizó la noticia, por favor verificar los datos",
        )
        return super(ModificarNoticia, self).form_invalid(form)


class EliminarNoticia(RedirectView):
    permanent = False
    query_string = True
    # login_url = reverse_lazy('login')

    @verificar_modulo_incluido("Noticias")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(EliminarNoticia, self).dispatch(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        try:
            funcion = Noticia.objects.get(pk=self.kwargs["pk"])
            funcion.delete()
            messages.success(
                self.request, "La noticia se ha eliminado satisfactoriamente"
            )
        except ObjectDoesNotExist:
            messages.error(self.request, "Error, la noticia a eliminar no existe")
        self.url = reverse_lazy("listado_noticias")
        return super(EliminarNoticia, self).get_redirect_url(*args, **kwargs)


# Create your views here.


class ListarNoticias(ListView):
    model = Noticia
    template_name = "listado_noticias.html"
    context_object_name = "lista_noticias"
    # login_url = reverse_lazy('login')

    @verificar_modulo_incluido("Noticias")
    @verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ListarNoticias, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarNoticias, self).get_context_data(**kwargs)
        context["titulo"] = "Listado de noticias"
        context["lista_encabezados"] = ["Título", "Descripción", "Fecha", "Opciones"]
        context["tipoLista"] = "Noticia"
        context["accion"] = "Listado"

        return context


def detalle_noticia(request, id_noticia=None):
    if not request.user.is_anonymous():
        template = "detalle_noticia.html"
    else:
        template = "vista_publica/detalle_noticia.html"
    try:
        noticia = Noticia.objects.get(pk=id_noticia)
    except ObjectDoesNotExist:
        messages.error(request, "Error, no existe la noticia")
        return redirect("inicio")
    contexto = {
        "noticia": noticia,
        "titulo": "Detalle de la noticia",
        "accion": "Detalle",
    }

    return render(request, template, contexto)
