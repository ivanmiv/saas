from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse_lazy

from django_tenants.test.cases import TenantTestCase
from django_tenants.test.client import TenantClient
from datetime import date

from proyectoWWW.utilidades import obtener_mensajes_respuesta
from apps.empresa.models import Plan
from apps.Usuario.models import Usuario
from .models import Noticia


class RegistrarNoticiaTestCase(TenantTestCase):
    def setup_tenant(self, tenant):
        from django.utils import timezone

        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return "localhost"

    @staticmethod
    def get_test_schema_name():
        return "tester"

    def setUp(self):
        super(RegistrarNoticiaTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user(
            "operador",
            "root@gmail.com",
            password,
            cedula=123,
            edad=30,
            telefono=5555555,
        )
        user.set_password(password)
        user.first_name = "cliente"
        user.is_superuser = False
        user.is_staff = False
        user.cargo = "Operador"
        user.save()

        password = "gerente123"
        gerente = Usuario.objects.create_user(
            "gerente", "root@gmail.com", password, cedula=456, edad=30, telefono=5555555
        )
        gerente.set_password(password)
        gerente.first_name = "gerente"
        gerente.is_superuser = False
        gerente.is_staff = False
        gerente.cargo = "Gerente"
        gerente.save()

    def test_registro_noticia(self):
        self.cliente.login(username="operador", password="operador123")
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "Noticia 1",
                "descripcion": "Esta es una noticia de prueba",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("La noticia se ha guardado satisfactoriamente", mensaje)

    def test_registro_noticia_sin_login(self):
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "Noticia 1",
                "descripcion": "Esta es una noticia de prueba",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("Para acceder a la página solicitada requiere loguearse", mensaje)

    def test_registro_sin_titulo(self):
        self.cliente.login(username="operador", password="operador123")
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "",
                "descripcion": "Esta es una noticia de prueba",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró la noticia, por favor verificar los datos", mensaje
        )

    def test_registro_sin_descripcion(self):
        self.cliente.login(username="operador", password="operador123")
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "Noticia 1",
                "descripcion": "",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró la noticia, por favor verificar los datos", mensaje
        )

    def test_registro_sin_imagen(self):
        self.cliente.login(username="operador", password="operador123")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "Noticia 1",
                "descripcion": "Esta es una noticia de prueba",
                "imagen": "",
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró la noticia, por favor verificar los datos", mensaje
        )

    def test_registro_sin_fecha(self):
        self.cliente.login(username="operador", password="operador123")
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("noticia_registro"),
            {
                "titulo": "Noticia 1",
                "descripcion": "Esta es una noticia de prueba",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": "",
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn(
            "Error, no se registró la noticia, por favor verificar los datos", mensaje
        )


class ModificarNoticiaTestCase(TenantTestCase):
    def setup_tenant(self, tenant):
        from django.utils import timezone

        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return "localhost"

    @staticmethod
    def get_test_schema_name():
        return "tester"

    def setUp(self):
        super(ModificarNoticiaTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        Usuario.objects.all().delete()

        password = "operador123"
        user = Usuario.objects.create_user(
            "operador",
            "root@gmail.com",
            password,
            cedula=123,
            edad=30,
            telefono=5555555,
        )
        user.set_password(password)
        user.first_name = "cliente"
        user.is_superuser = False
        user.is_staff = False
        user.cargo = "Operador"
        user.save()

        password = "gerente123"
        gerente = Usuario.objects.create_user(
            "gerente", "root@gmail.com", password, cedula=456, edad=30, telefono=5555555
        )
        gerente.set_password(password)
        gerente.first_name = "gerente"
        gerente.is_superuser = False
        gerente.is_staff = False
        gerente.cargo = "Gerente"
        gerente.save()

        noticia = Noticia.objects.create(
            titulo="Noticia 1",
            descripcion="Noticia de prueba",
            imagen="static/img/thumbs/img1.jpg",
            fecha=date(2018, 6, 1),
        )
        noticia.save()

    def test_modificar_noticia(self):
        self.cliente.login(username="operador", password="operador123")
        upload_file = open("static/img/thumbs/img1.jpg", "rb")
        respuesta = self.cliente.post(
            reverse_lazy("modificar_noticia", kwargs={"pk": 1}),
            {
                "titulo": "Noticia 2",
                "descripcion": "Esta es una modificacion",
                "imagen": SimpleUploadedFile(upload_file.name, upload_file.read()),
                "fecha": date(2018, 6, 1),
            },
        )
        mensaje = obtener_mensajes_respuesta(respuesta)
        self.assertIn("La noticia se ha actualizado satisfactoriamente", mensaje)
