from django.db import transaction, IntegrityError
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, UpdateView, FormView
from django.core.exceptions import ObjectDoesNotExist

from .views import *
from .forms import *
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist

from .models import ParticipantesEventos
from apps.Usuario.models import Usuario
from apps.Evento.models import Evento
from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)


@verificar_modulo_incluido("Participantes")
@vigencia_disponible_empresa
def registro(request):
    if request.method == "POST":
        form = ParticipanteForm(request.POST)
        if form.is_valid():
            participante = form.save(commit=False)
            participante.cargo = "Participante"
            participante.save()
            messages.success(request, "El registro se ha completado exitosamente.")
            return redirect("inicio")
        else:
            messages.error(
                request,
                "No se pudo realizar el registro, por favor verificar los campos en rojo.",
            )
    else:
        form = ParticipanteForm()

    return render(request, "participante_registro.html", {"form": form})


@verificar_modulo_incluido("Participantes")
@verificar_cargo(cargos_permitidos=["Operador"])
@vigencia_disponible_empresa
def generarEscarapela(request, id_participante, id_evento):
    try:
        evento = Evento.objects.get(id=id_evento)
        particip = Usuario.objects.get(id=id_participante)
        asistencia = ParticipantesEventos.objects.get(
            participante=particip, evento=evento
        )
        asistencia.estado_asistencia = "ASISTIÓ"
        asistencia.save()

    except ObjectDoesNotExist:
        messages.error(request, "Error, el participante no existe")
        return redirect("inicio")

    contexto = {
        "titulo": "Generar escarapela",
        "tipo": "Participante",
        "accion": "Generar",
        "asistencias": asistencia,
    }
    return render(request, "generar_escarapela.html", contexto)


@verificar_modulo_incluido("Participantes")
@verificar_cargo(cargos_permitidos=["Participante"])
@vigencia_disponible_empresa
def preinscripcion(request):
    evento = Evento.obtener_evento_sesion(request)
    participante = request.user
    if ParticipantesEventos.determinar_participante_evento(evento, participante):
        messages.warning(request, "Ya se encuentra registrado en este evento.")
        return redirect("evento_detalle_evento", evento.id)

    if not evento:
        messages.error(
            request, "Error, debe seleccionar un evento para poder preinscribirse"
        )
        return redirect("inicio")
    else:
        if request.method == "POST":
            from datetime import date

            try:
                with transaction.atomic():
                    fecha_actual = date.today()
                    if fecha_actual <= evento.fecha_fin:
                        asistencia = ParticipantesEventos.objects.create(
                            evento=evento, participante=participante
                        )
                        if participante.fecha_fin_membresia:
                            asistencia.valor_pagado = (
                                (evento.precio - evento.precio * 0.3)
                                if fecha_actual <= participante.fecha_fin_membresia
                                else evento.precio
                            )
                        else:
                            asistencia.valor_pagado = evento.precio
                        asistencia.save()
                        messages.success(
                            request,
                            "El participante se ha pre-inscrito satisfactoriamente",
                        )
                        evento.enviar_correo_preinscripcion(request, participante)
                    else:
                        messages.error(request, "El evento ya finalizo")
            except IntegrityError as e:
                messages.error(
                    request, "Error, fallo durante la creación del participante"
                )

            return redirect("inicio")

    return redirect("evento_detalle_evento", evento.id)


class ingresarDatosValidacion(FormView):
    template_name = "formulario_peticion_modificar.html"
    form_class = ClaveForm
    success_url = reverse_lazy("participante_modificar_participante")

    def form_valid(self, form):

        clave = form.cleaned_data["clave"]
        participante = Usuario.buscar_participante_por_clave(clave)
        if participante:
            messages.success(
                self.request, "Ingreso exitoso, ahora puede modificar su información"
            )
            self.request.session["id_participante"] = participante.pk
            return super(ingresarDatosValidacion, self).form_valid(form)
        else:
            error = "Número de registro inválido"
        form._errors["clave"] = [error]
        messages.error(self.request, error)
        return super(ingresarDatosValidacion, self).form_valid(form)

    def form_invalid(self, form):
        messages.add_message(
            self.request, messages.ERROR, "Código de validación incorrecto"
        )
        return super(ingresarDatosValidacion, self).form_invalid(form)


@verificar_modulo_incluido("Participantes")
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
@vigencia_disponible_empresa
def modificarParticipante(request):
    participante = Usuario.obtener_participante_sesion(request)

    if not participante:
        messages.error(request, "Error, no se ha encontrado el participante")
        return redirect("inicio")
    else:
        if request.method == "POST":
            form = ParticipanteModificarForm(request.POST, instance=participante)
            if form.is_valid():
                participante = form.save()
                messages.success(
                    request, "Su información personal ha sido modificada exitosamente"
                )
                return redirect("inicio")
            else:
                messages.error(
                    request,
                    "Error, no ha podido realizar la modificación, por favor verificar los datos",
                )
                return redirect("participante_modificar_participante")
        else:
            form = ParticipanteModificarForm(instance=participante)

    contexto = {
        "titulo": "Modificación de participante",
        "tipo": "Participante",
        "accion": "Modificacion",
        "form": form,
    }
    return render(request, "participante_form_modificar.html", contexto)


class ListarParticipantesPre(ListView):
    model = Usuario
    template_name = "listado_participantes_pre.html"
    context_object_name = "lista_elementos"
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Participantes")
    @verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ListarParticipantesPre, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarParticipantesPre, self).get_context_data(**kwargs)
        context["titulo"] = "Listado de Participantes Pre-inscritos"
        context["lista_encabezados"] = ["Nombres", "Apellidos", "Nº de documento"]
        context["tipoLista"] = "Participante"
        context["accion"] = "Listado"

        return context

    def get_queryset(self, **kwargs):
        queryset = super(ListarParticipantesPre, self).get_queryset(**kwargs)
        queryset = queryset.filter(cargo="Participante")
        return queryset


@verificar_modulo_incluido("Participantes")
@verificar_cargo(cargos_permitidos=["Participante"])
@vigencia_disponible_empresa
def adquirir_membresia(request, tipo_membresia):
    """
    :param request:
    :param id_participante:
    :param tipo_membresia: Valor númerico donde cada valor representa la duración de la membresia. 1: Un mes
        2: 3 meses, 3: 6 meses y 4: un año
    :return:
    """
    import datetime
    from dateutil.relativedelta import relativedelta

    usuario = request.user
    if not usuario:
        messages.error(request, "El usuario solicitado no existe")
        return redirect("inicio")

    usuario.fecha_fin_membresia = usuario.tiempo_membresia(tipo_membresia)
    usuario.save()
    messages.success(request, "Se ha adquirido exitosamente la membresia")

    return redirect("dashboard")
