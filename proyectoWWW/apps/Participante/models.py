from django.db import models

from apps.Evento.models import Evento
from simple_history.models import HistoricalRecords


class Registro(models.Model):
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)
    historia = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True


#
# class Participante(Registro):
#     numero_documento = models.BigIntegerField(verbose_name='número de documento de identidad')
#     primer_nombre = models.CharField(max_length=50,verbose_name='primer nombre')
#     segundo_nombre = models.CharField(max_length=50,verbose_name='segundo nombre',blank=True)
#     primer_apellido = models.CharField(max_length=50,verbose_name='primer apellido')
#     segundo_apellido = models.CharField(max_length=50,verbose_name='segundo apellido',blank=True)
#     telefono = models.BigIntegerField(verbose_name='teléfono fijo',blank=True,default=0)
#     celular = models.BigIntegerField(verbose_name='celular',blank=True,default=0)
#     email = models.EmailField(verbose_name='correo electrónico')
#     direccion = models.CharField(max_length=100,verbose_name='dirección de la residencia')
#     actividades = models.ManyToManyField('Actividad.Actividad',related_name='actividades')
#
#     def __str__(self):
#         return ("%s %s %s %s" %(self.primer_apellido,self.segundo_nombre or "",self.primer_apellido,self.segundo_apellido or "")).strip()
#
#
#     def numero_inscripcion(self):
#         mihash = (self.numero_documento*44383)%1000000007
#         clave = "%d-%d"%(self.id,mihash)
#         return clave
#
#     @staticmethod
#     def obtener_participante(id_participante):
#         try:
#             return Participante.objects.get(id=id_participante)
#         except Exception:
#             return None
#
#     @staticmethod
#     def obtener_participante_sesion(request):
#         id_participante = request.session.get('id_participante')
#         if not id_participante:
#             return None
#         return Participante.obtener_participante(id_participante)
#
#     @staticmethod
#     def buscar_participante_por_clave(clave):
#         try:
#             mi_id, mi_hash = map(int, clave.split("-"))
#             aspirante = Participante.objects.get(id=mi_id)
#             if aspirante.numero_inscripcion() == clave:
#                 return aspirante
#             else:
#                 return None
#         except Exception as ex:
#             return None


class ParticipantesEventos(Registro):
    ESTADO_CHOICES = (("PREINSCRITO", "Pre-inscrito"), ("INSCRITO", "Inscrito"))

    ESTADO_ASISTENCIA_CHOICES = (("ASISTIÓ", "Asistió"), ("NO ASISTIÓ", "No asistió"))

    evento = models.ForeignKey(Evento, related_name="participantes_evento")
    participante = models.ForeignKey("Usuario.Usuario")
    estado_participante = models.CharField(
        max_length=15,
        verbose_name="estado del participante",
        choices=ESTADO_CHOICES,
        default="PREINSCRITO",
    )
    estado_asistencia = models.CharField(
        max_length=15,
        verbose_name="estado de la asistencia",
        choices=ESTADO_ASISTENCIA_CHOICES,
        default="NO ASISTIÓ",
    )
    valor_pagado = models.PositiveIntegerField(
        verbose_name="valor pagado", blank=True, null=True
    )

    @staticmethod
    def determinar_participante_evento(evento, participante):
        try:
            ParticipantesEventos.objects.get(evento=evento, participante=participante)
            return True
        except ParticipantesEventos.DoesNotExist:
            return False
