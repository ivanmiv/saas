from django.contrib.auth.forms import UserCreationForm
from captcha.fields import ReCaptchaField
from django import forms
from apps.Usuario.models import Usuario


class ParticipanteForm(UserCreationForm):
    captcha = ReCaptchaField(label="Debe realizar la verificación captcha.")

    class Meta:
        model = Usuario
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "edad",
            "cedula",
            "telefono",
            "sexo",
            "imagen",
        )


class ParticipanteModificarForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "edad",
            "cedula",
            "telefono",
            "sexo",
            "imagen",
        )


class ClaveForm(forms.Form):
    clave = forms.CharField(
        label="",
        max_length=100,
        widget=forms.TextInput(attrs={"placeholder": "ingresar"}),
    )
