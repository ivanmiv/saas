from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    FormView,
    TemplateView,
)

from .forms import (
    RegistroUsuarioForm,
    LoginForm,
    RestablecerContrasenaForm,
    ModificarUsuarioForm,
    ClienteForm,
    ModificarPerfilForm,
)
from .models import Usuario
from proyectoWWW.utilidades import (
    verificar_cargo,
    vigencia_disponible_empresa,
    verificar_modulo_incluido,
)


@verificar_modulo_incluido("Usuarios")
@verificar_cargo(
    cargos_permitidos=[
        "Administrador",
        "Operador",
        "Gerente",
        "Participante",
        "Cliente",
    ]
)
@vigencia_disponible_empresa
def perfil_usuario(request, id_user):
    try:
        usuario = Usuario.objects.get(id=id_user)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el usuario no existe")
        return redirect("inicio")
    contexto = {
        "titulo": "Perfil de usuario",
        "tipo": "Información",
        "usuario": usuario,
    }
    return render(request, "Usuario/perfil_usuario.html", contexto)


@verificar_modulo_incluido("Usuarios")
@verificar_cargo(cargos_permitidos=["Administrador"])
@vigencia_disponible_empresa
def cambiar_estado(request, id_user):
    try:
        usuario = Usuario.objects.get(id=id_user)
    except ObjectDoesNotExist:
        messages.error(request, "Error, el usuario a modificar no existe")
        return redirect("usuario_listado_usuario")

    if usuario.is_active == False:
        usuario.is_active = True
        messages.success(request, "El usuario ha sido habilitado con éxito")
    else:
        usuario.is_active = False
        messages.success(request, "El usuario ha sido inhabilitado con éxito")
    usuario.save()
    return redirect("usuario_listado_usuario")


class RegistroUsuario(CreateView):
    form_class = RegistroUsuarioForm
    template_name = "Usuario/templateForm.html"
    success_url = reverse_lazy("usuario_listado_usuario")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(cargos_permitidos=["Administrador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        if (
            request.tenant.schema_name != "public"
            and Usuario.objects.all().count() >= request.tenant.plan.maximo_usuarios
        ):
            messages.error(
                request,
                "La empresa ha alcanzado el límite de usuarios permitidos, por favor contacte con el administrador",
            )
            return redirect(reverse_lazy("usuario_listado_usuario"))

        return super(RegistroUsuario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegistroUsuario, self).get_context_data(**kwargs)
        context["titulo"] = "Registro de Usuario"
        context["tipo"] = "Registro"
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(self.request, "El usuario ha sido creado con éxito")
        return super(RegistroUsuario, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request, "Error, no se ha podido crear el usuario, verifique los datos"
        )
        return super(RegistroUsuario, self).form_invalid(form)

    def get_form_kwargs(self):
        kwargs = super(RegistroUsuario, self).get_form_kwargs()
        kwargs.update({"schema": self.request.tenant.schema_name})
        return kwargs


class ModificarUsuario(UpdateView):
    model = Usuario
    form_class = ModificarUsuarioForm
    template_name = "Usuario/modificar_usuario.html"
    success_url = reverse_lazy("usuario_listado_usuario")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(cargos_permitidos=["Administrador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ModificarUsuario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModificarUsuario, self).get_context_data(**kwargs)
        context["titulo"] = "Modificación de usuario"
        context["tipo"] = "Usuario"
        return context

    def get(self, *args, **kwargs):
        try:
            Usuario.objects.get(pk=self.kwargs["pk"])
        except ObjectDoesNotExist:
            messages.error(self.request, "Error, el usuario a modificar no existe")
            return redirect(reverse_lazy("usuario_listado_usuario"))
        return super(ModificarUsuario, self).get(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        messages.success(
            self.request, "El usuario se ha actualizado satisfactoriamente"
        )
        return super(ModificarUsuario, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se pudo modificar la información del usuario, por favor verificar los datos",
        )
        return super(ModificarUsuario, self).form_invalid(form)

    def get_form_kwargs(self):
        kwargs = super(ModificarUsuario, self).get_form_kwargs()
        kwargs.update({"schema": self.request.tenant.schema_name})
        return kwargs


class ListarUsuario(ListView):
    model = Usuario
    template_name = "Usuario/listado.html"
    context_object_name = "lista_usuario"
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(cargos_permitidos=["Administrador", "Gerente"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ListarUsuario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListarUsuario, self).get_context_data(**kwargs)
        context["titulo"] = "Listado de usuarios"
        context["lista_encabezados"] = [
            "Usuario",
            "Cédula",
            "Nombre completo",
            "Correo electrónico",
            "Cargo",
            "Estado",
            "Opciones",
        ]
        context["tipoLista"] = "Usuario"
        context["accion"] = "Listado"

        return context

    def get_queryset(self):
        queryset = Usuario.objects.all()
        if self.request.user.cargo in ["Gerente"]:
            queryset = queryset.exclude(cargo="Administrador")
        else:
            queryset = queryset.exclude(is_superuser=True)
        return queryset


class Login(FormView):
    form_class = LoginForm
    template_name = "Usuario/login.html"

    def form_valid(self, form):
        mensaje = ""
        user = authenticate(
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"],
        )
        if user is not None:
            if user.is_active:
                login(self.request, user)
                usuario = get_object_or_404(Usuario, pk=self.request.user.pk)
                if usuario.cargo == "Participante":
                    self.success_url = reverse_lazy("inicio")
                else:
                    self.success_url = reverse_lazy("dashboard")
                messages.add_message(
                    self.request, messages.SUCCESS, "Sesión iniciada correctamente."
                )
                return super(Login, self).form_valid(form)
            else:
                mensaje = ("El usuario %s no esta activo") % (user.username)
        else:
            mensaje = "El usuario no existe o la contraseña es incorrecta"
        form.add_error("username", mensaje)
        messages.add_message(self.request, messages.ERROR, mensaje)
        return super(Login, self).form_invalid(form)

    def form_invalid(self, form):
        if self.request.POST.get("g-recaptcha-response") != None:
            messages.add_message(
                self.request,
                messages.ERROR,
                "Debe realizar la verificación del reCAPTCHA",
            )
        else:
            messages.add_message(
                self.request,
                messages.ERROR,
                "Ingrese un usuario y una contraseña para acceder al sistema",
            )
        return super(Login, self).form_invalid(form)


class InicioUsuario(LoginRequiredMixin, TemplateView):
    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(
        cargos_permitidos=[
            "Administrador",
            "Gerente",
            "Operador",
            "Cliente",
            "Participante",
        ]
    )
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(InicioUsuario, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InicioUsuario, self).get_context_data(**kwargs)
        self.template_name = self.request.user.obtener_pagina_dashboard()
        context.update(self.request.user.datos_dashboard())
        return context


@login_required
def Logout(request):
    logout(request)
    return redirect("login")


class RestablecerContrasena(LoginRequiredMixin, FormView):
    model = Usuario
    form_class = RestablecerContrasenaForm
    template_name = "Usuario/restablecer_contrasena.html"
    success_url = reverse_lazy("usuario_listado_usuario")
    login_url = reverse_lazy("login")

    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(cargos_permitidos=["Administrador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        try:
            Usuario.objects.get(pk=kwargs["pk"])
        except Usuario.DoesNotExist:
            messages.error(self.request, "El usuario solicitado no existe")
            return redirect("usuario_listado_usuario")
        return super(RestablecerContrasena, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RestablecerContrasena, self).get_context_data(**kwargs)
        context["usuario"] = Usuario.objects.get(pk=self.kwargs["pk"])
        return context

    def form_valid(self, form):
        password = form.cleaned_data["password1"]
        usuario = Usuario.objects.get(pk=self.kwargs["pk"])
        usuario.set_password(password)
        usuario.save()
        messages.success(
            self.request, "Se ha editado exitosamente la contraseña del usuario"
        )
        return super(RestablecerContrasena, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "No se logró editar la contraseña del usuario")
        return super(RestablecerContrasena, self).form_invalid(form)


@verificar_modulo_incluido("Usuarios")
@vigencia_disponible_empresa
def registro_publico_cliente(request):
    if (
        request.tenant.schema_name != "public"
        and Usuario.objects.all().count() >= request.tenant.plan.maximo_usuarios
    ):
        messages.error(
            request,
            "La empresa ha alcanzado el límite de usuarios permitidos, por favor contacte con el administrador",
        )
        return redirect(reverse_lazy("usuario_listado_usuario"))

    if request.method == "POST":
        form = ClienteForm(request.POST)
        if form.is_valid():
            participante = form.save(commit=False)
            participante.cargo = "Cliente"
            participante.save()
            messages.success(request, "El registro se ha completado exitosamente.")
            return redirect("inicio")
        else:
            messages.error(
                request,
                "No se pudo realizar el registro, por favor verificar los campos en rojo.",
            )
    else:
        form = ClienteForm()

    return render(request, "Usuario/cliente_registro_publico.html", {"form": form})


class ModificarPerfil(UpdateView):
    model = Usuario
    form_class = ModificarPerfilForm
    template_name = "Usuario/modificar_perfil.html"
    success_url = reverse_lazy("usuario_perfil_usuario")

    @verificar_modulo_incluido("Usuarios")
    @verificar_cargo(
        cargos_permitidos=[
            "Administrador",
            "Gerente",
            "Operador",
            "Cliente",
            "Participante",
        ]
    )
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ModificarPerfil, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModificarPerfil, self).get_context_data(**kwargs)
        pk = self.kwargs.get("pk", 0)
        usuario = self.model.objects.get(id=pk)

        if "form" not in context:
            context["form"] = self.form_class(instance=usuario)
        context["id"] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id_usuario = kwargs["pk"]
        usuario = self.model.objects.get(id=id_usuario)
        form_one = self.form_class(request.POST, instance=usuario)

        if form_one.is_valid():
            form_one.save()
            messages.success(self.request, "El usuario ha sido modificado con éxito")
            return redirect("usuario_perfil_usuario", id_user=id_usuario)
        else:
            return redirect("usuario_perfil_modificar", pk=id_usuario)
