import os

from django.urls import reverse_lazy
from django_tenants.test.cases import TenantTestCase
from django_tenants.test.client import TenantClient

from apps.empresa.models import Plan
from proyectoWWW.utilidades import obtener_mensajes_respuesta
from .models import *


class LoginTestCase(TenantTestCase):
    def setup_tenant(self, tenant):
        from django.utils import timezone

        Plan.crear_planes_iniciales()
        Usuario.crear_usuario_inicial()
        tenant.plan = Plan.objects.all()[0]
        tenant.cliente = Usuario.objects.all()[0]
        tenant.vigencia = timezone.now() + timezone.timedelta(days=1)
        tenant.nombre = "Prueba"

    @staticmethod
    def get_test_tenant_domain():
        return "localhost"

    @staticmethod
    def get_test_schema_name():
        return "tester"

    def setUp(self):
        super(LoginTestCase, self).setUp()
        self.cliente = TenantClient(self.tenant)
        os.environ["RECAPTCHA_TESTING"] = "True"

        Usuario.objects.all().delete()

        password = "admin123"
        user = Usuario.objects.create_user(
            "admin", "root@gmail.com", password, cedula=123, edad=30, telefono=5555555
        )
        user.set_password(password)
        user.first_name = "Administrador"
        user.is_superuser = True
        user.is_staff = True
        user.cargo = "Administrador"
        user.save()

    def test_inicio_sesion_correcto(self):
        respuesta = self.cliente.post(
            reverse_lazy("login"),
            {
                "username": "admin",
                "password": "admin123",
                "g-recaptcha-response": "PASSED",
            },
        )
        self.assertRedirects(respuesta, reverse_lazy("dashboard"), 302, 200)

    def test_inicio_sesion_usuario_incorrecto(self):
        respuesta = self.cliente.post(
            reverse_lazy("login"),
            {
                "username": "asdasda",
                "password": "admin123",
                "g-recaptcha-response": "PASSED",
            },
        )
        mensajes = obtener_mensajes_respuesta(respuesta)

        self.assertIn("El usuario no existe o la contraseña es incorrecta", mensajes)

    def test_inicio_sesion_password_incorrecta(self):
        respuesta = self.cliente.post(
            reverse_lazy("login"),
            {
                "username": "admin",
                "password": "valor 123",
                "g-recaptcha-response": "PASSED",
            },
        )
        mensajes = obtener_mensajes_respuesta(respuesta)

        self.assertIn("El usuario no existe o la contraseña es incorrecta", mensajes)

    def tearDown(self):
        super(LoginTestCase, self).tearDown()
        del os.environ["RECAPTCHA_TESTING"]
