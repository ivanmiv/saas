from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import *
from captcha.fields import ReCaptchaField


class RegistroUsuarioForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        schema = kwargs.pop("schema", None)
        super(RegistroUsuarioForm, self).__init__(*args, **kwargs)
        if schema == "public":
            self.fields["cargo"].choices = [
                self.fields["cargo"].choices[i] for i in [0, 1, 4]
            ]
        else:
            self.fields["cargo"].choices = [
                self.fields["cargo"].choices[i] for i in [0, 1, 2, 3, 5]
            ]

    password1 = forms.CharField(
        max_length=30, required=True, label="Contraseña", widget=forms.PasswordInput
    )

    class Meta:
        model = Usuario
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "cargo",
            "edad",
            "cedula",
            "telefono",
            "sexo",
            "imagen",
        )
        widgets = {
            "first_name": forms.DateInput(attrs={"required": True}),
            "last_name": forms.DateInput(attrs={"required": True}),
            "sexo": forms.Select(attrs={"class": "select2"}),
            "cargo": forms.Select(attrs={"class": "select2"}),
            "estado": forms.Select(attrs={"class": "select2"}),
        }


class ModificarUsuarioForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        schema = kwargs.pop("schema", None)
        super(ModificarUsuarioForm, self).__init__(*args, **kwargs)
        if schema == "public":
            self.fields["cargo"].choices = [
                self.fields["cargo"].choices[i] for i in [0, 1, 4]
            ]
        else:
            self.fields["cargo"].choices = [
                self.fields["cargo"].choices[i] for i in [0, 1, 2, 3, 5]
            ]

    class Meta:
        model = Usuario
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "cargo",
            "edad",
            "cedula",
            "telefono",
            "sexo",
            "imagen",
        )
        widgets = {
            "first_name": forms.DateInput(attrs={"required": True}),
            "last_name": forms.DateInput(attrs={"required": True}),
            "sexo": forms.Select(attrs={"class": "select2"}),
            "cargo": forms.Select(attrs={"class": "select2"}),
            "estado": forms.Select(attrs={"class": "select2"}),
        }


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                "id": "usernameInput",
                "placeholder": "Nombre de usuario",
                "class": "form-control",
            }
        ),
    )
    password = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                "type": "password",
                "id": "passwordInput",
                "placeholder": "Contraseña",
                "class": "form-control",
            }
        ),
    )
    captcha = ReCaptchaField()


class RestablecerContrasenaForm(forms.Form):
    password1 = forms.CharField(
        label="Contraseña nueva",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                "type": "password",
                "placeholder": "Contraseña",
                "data-parsley-contrasena": "",
                "class": "form-control",
            }
        ),
    )

    password2 = forms.CharField(
        label="Confirmar contraseña nueva",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                "type": "password",
                "placeholder": "Confirmar contraseña",
                "data-parsley-confirmacion": "id_password1",
                "class": "form-control",
            }
        ),
    )

    def clean(self):
        form_data = self.cleaned_data
        if form_data["password1"] != form_data["password2"]:
            self._errors["password2"] = ["Las contraseñas no coinciden"]
        return form_data


class ModificarPerfilForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ("first_name", "last_name", "email", "edad", "telefono", "sexo")

        labels = {
            "first_name": "Nombres",
            "last_name": "Apellidos",
            "email": "Correo electrónico",
            "edad": "Edad",
            "telefono": "Teléfono",
            "sexo": "Sexo",
        }

        widgets = {
            "first_name": forms.DateInput(),
            "last_name": forms.DateInput(),
            "email": forms.DateInput(),
            "edad": forms.DateInput(),
            "telefono": forms.DateInput(),
            "sexo": forms.Select(attrs={"class": "select2"}),
        }


class ClienteForm(UserCreationForm):
    captcha = ReCaptchaField(label="Debe realizar la verificación captcha.")

    class Meta:
        model = Usuario
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "edad",
            "cedula",
            "telefono",
            "sexo",
            "imagen",
        )
