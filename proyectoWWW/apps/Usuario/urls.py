from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^login$", Login.as_view(), name="login"),
    url(r"^logout$", Logout, name="logout"),
    url(r"^dashboard$", InicioUsuario.as_view(), name="dashboard"),
    url(r"^registro$", RegistroUsuario.as_view(), name="usuario_registro_usuario"),
    url(
        r"^modificar/(?P<pk>\d+)$",
        ModificarUsuario.as_view(),
        name="usuario_modificar_usuario",
    ),
    url(r"^listar$", ListarUsuario.as_view(), name="usuario_listado_usuario"),
    url(r"^estado/(?P<id_user>\d+)$", cambiar_estado, name="usuario_estado_usuario"),
    url(r"^perfil/(?P<id_user>\d+)$", perfil_usuario, name="usuario_perfil_usuario"),
    url(
        r"^modificar_perfil/(?P<pk>\d+)$",
        ModificarPerfil.as_view(),
        name="usuario_perfil_modificar",
    ),
    url(
        r"^restablecer_contrasena/(?P<pk>\d+)$",
        RestablecerContrasena.as_view(),
        name="usuarios_restablecer_contrasena",
    ),
    url(
        r"^inscripcion-cliente$",
        registro_publico_cliente,
        name="usuario_registro_publico_cliente",
    ),
]
