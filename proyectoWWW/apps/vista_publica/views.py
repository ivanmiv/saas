from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from apps.Usuario.models import Usuario
from apps.Evento.models import Evento
from apps.Noticia.models import Noticia
from apps.empresa.models import Plan


def finalizar_sesiones(request):
    evento = Evento.obtener_evento_sesion(request)
    participante = Usuario.obtener_participante_sesion(request)
    if evento:
        del request.session["id_evento"]
    if participante:
        del request.session["id_participante"]


def Inicio(request):
    Usuario.crear_usuario_inicial()
    if request.tenant.schema_name == "public":
        Plan.crear_planes_iniciales()
        return render(
            request,
            "vista_publica/inicio_publico.html",
            {"titulo": "Inicio", "planes": Plan.objects.all()},
        )
    else:
        finalizar_sesiones(request)

        lista_eventos = Evento.objects.filter(estado="ACTIVO")
        pagina = request.GET.get("pagina", 1)
        consulta_noticias = Noticia.objects.all().order_by("-fecha")[:5]

        paginador = Paginator(lista_eventos, 10)
        try:
            eventos = paginador.page(pagina)
        except PageNotAnInteger:
            eventos = paginador.page(1)
        except EmptyPage:
            eventos = paginador.page(paginador.num_pages)

    return render(
        request,
        "vista_publica/inicio.html",
        {"eventos": eventos, "noticias": consulta_noticias, "titulo": "Inicio"},
    )
