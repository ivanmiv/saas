from django.apps import AppConfig


class VistaPublicaConfig(AppConfig):
    name = "vista_publica"
