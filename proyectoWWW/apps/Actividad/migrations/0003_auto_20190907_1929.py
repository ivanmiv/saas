# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2019-09-08 00:29
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("Actividad", "0002_historicalactividad_evento"),
        ("Evento", "0001_initial"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name="historicalactividad",
            name="history_user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="actividad",
            name="evento",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="actividades",
                to="Evento.Evento",
            ),
        ),
    ]
