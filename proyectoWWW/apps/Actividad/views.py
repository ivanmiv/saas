from __future__ import absolute_import
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.views.generic import ListView, RedirectView
from django.views.generic.edit import CreateView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404

from apps.Actividad.forms import ActividadForm
from apps.Actividad.models import Actividad
from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)


class ActividadList(ListView):
    model = Actividad
    template_name = "Actividad/actividad_list.html"
    context_object_name = "lista_elementos"

    @verificar_modulo_incluido("Actividades")
    @verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ActividadList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ActividadList, self).get_context_data(**kwargs)
        context["titulo"] = "Listado de actividades"
        context["tipoLista"] = "Actividad"
        context["accion"] = "Listado"

        return context


class ActividadCreate(CreateView):
    form_class = ActividadForm
    template_name = "Actividad/actividad_crear.html"
    success_url = reverse_lazy("actividad_crear")

    @verificar_modulo_incluido("Actividades")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ActividadCreate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ActividadCreate, self).get_context_data(**kwargs)
        context["titulo"] = "Registro de actividad"
        context["tipo"] = "Actividad"
        context["accion"] = "Registro"
        return context

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "La actividad se ha guardado satisfactoriamente")
        return super(ActividadCreate, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se registró la actividad, por favor verificar los datos",
        )
        return super(ActividadCreate, self).form_invalid(form)


class ActividadModificar(UpdateView):
    model = Actividad
    form_class = ActividadForm
    template_name = "Actividad/actividad_crear.html"
    success_url = reverse_lazy("actividad_listar")

    # login_url = reverse_lazy('login')

    @verificar_modulo_incluido("Actividades")
    @verificar_cargo(cargos_permitidos=["Operador"])
    @vigencia_disponible_empresa
    def dispatch(self, request, *args, **kwargs):
        return super(ActividadModificar, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ActividadModificar, self).get_context_data(**kwargs)
        context["titulo"] = "Modificación de actividad"
        context["tipo"] = "Actividad"
        context["accion"] = "Modificación"
        return context

    def get(self, *args, **kwargs):
        try:
            Actividad.objects.get(pk=self.kwargs["pk"])
        except ObjectDoesNotExist:
            messages.error(self.request, "Error, la actividad a modificar no existe")
            return redirect(reverse_lazy("actividad_listar"))
        return super(ActividadModificar, self).get(*args, **kwargs)

    def form_valid(self, form):
        form.save()
        messages.success(
            self.request, "La actividad se ha actualizado satisfactoriamente"
        )
        return super(ActividadModificar, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(
            self.request,
            "Error, no se actualizó la actividad, por favor verificar los datos",
        )
        return super(ActividadModificar, self).form_invalid(form)


@verificar_modulo_incluido("Actividades")
@verificar_cargo(cargos_permitidos=["Gerente"])
@vigencia_disponible_empresa
def ActividadDesactivar(request, id_actividad):
    actividad = get_object_or_404(Actividad, id=id_actividad)

    if actividad.estado == "INACTIVO":
        actividad.estado = "ACTIVO"
    else:
        actividad.estado = "INACTIVO"
    actividad.save()
    messages.success(request, "Estado de la actividad modificado exitosamente")
    return redirect("actividad_listar")


@verificar_modulo_incluido("Actividades")
@verificar_cargo(cargos_permitidos=["Operador"])
@vigencia_disponible_empresa
def obtener_evento(request):
    from django.http import JsonResponse
    from apps.Evento.models import Evento

    if request.is_ajax():
        id_evento = request.POST.get("evento", None)
        evento = Evento.obtener_evento(id_evento)
        data = {
            "fecha_inicio": evento.fecha_inicio,
            "fecha_fin": evento.fecha_fin,
            "nombre": evento.nombre,
            "direccion": evento.address,
            "ubicacion": evento.ubicacion,
        }
        return JsonResponse(data)

    return redirect("actividad_listar")
