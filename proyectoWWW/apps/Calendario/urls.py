from django.conf.urls import url

from .views import *

urlpatterns = [
    url(
        r"^visualizar",
        VisualizarCalendario.as_view(),
        name="calendario_visualizar_calendario",
    ),
    url(r"^actualizar_datos", datos_calendario, name="calendario_datos_eventos"),
]
