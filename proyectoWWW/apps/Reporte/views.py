from django.db.models.aggregates import Count, Sum
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.db.models.functions.datetime import TruncDate

import datetime
from proyectoWWW.utilidades import (
    verificar_cargo,
    verificar_modulo_incluido,
    vigencia_disponible_empresa,
)

from apps.Participante.models import *
from .forms import *


def obtener_ingresos_eventos_top():
    import random

    etiquetas = []
    colores = []
    ingresos = []
    color_aleatorio = lambda: random.randint(0, 255)

    participantes_inscritos = ParticipantesEventos.objects.filter(
        estado_participante="INSCRITO"
    )
    top_5_eventos = (
        participantes_inscritos.values("evento__nombre")
        .annotate(ingresos=Sum("valor_pagado"))
        .order_by("-ingresos")[:5]
    )

    for evento in top_5_eventos:
        etiquetas.append(evento["evento__nombre"])
        ingresos.append(evento["ingresos"])
        colores.append(
            "#%02X%02X%02X" % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        )

    data = {
        "etiquetas": etiquetas,
        "valores": ingresos,
        "colores": colores,
        "ejeX": "Eventos",
        "ejeY": "Ingresos",
    }

    return data


def obtener_ingresos_eventos_todos():
    etiquetas = []
    ingresos = []

    participantes_inscritos = ParticipantesEventos.objects.filter(
        estado_participante="INSCRITO"
    )
    top_5_eventos = (
        participantes_inscritos.values("evento__nombre")
        .annotate(ingresos=Sum("valor_pagado"))
        .order_by("-ingresos")
    )

    for evento in top_5_eventos:
        etiquetas.append(evento["evento__nombre"])
        ingresos.append(evento["ingresos"])
    data = {"etiquetas": etiquetas, "valores": ingresos}

    return data


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_ingresos_evento_datos(request):
    datos_enviar = obtener_ingresos_eventos_top()
    return JsonResponse(datos_enviar)


@verificar_modulo_incluido("Reportes")
@vigencia_disponible_empresa
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_ingresos_evento(request):
    datos_tabla = obtener_ingresos_eventos_todos()
    etiquetas = datos_tabla["etiquetas"]
    valores = datos_tabla["valores"]
    data = []
    for iterador in range(len(etiquetas)):
        data.append({"label": etiquetas[iterador], "value": valores[iterador]})
    return render(request, "Reporte/reporte_ingresos_evento.html", {"datos": data})


# --------------------------------- Participantes por evento ----------------------------------------


def obtener_eventos_top_participantes():
    import random

    etiquetas = []
    colores = []
    participantes = []
    color_aleatorio = lambda: random.randint(0, 255)

    participantes_inscritos = ParticipantesEventos.objects.all()
    top_3_eventos = (
        participantes_inscritos.values("evento__nombre")
        .annotate(participantes=Count("evento__nombre"))
        .order_by("-participantes")[:5]
    )

    for evento in top_3_eventos:
        etiquetas.append(evento["evento__nombre"])
        participantes.append(evento["participantes"])
        colores.append(
            "#%02X%02X%02X" % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        )

    data = {
        "etiquetas": etiquetas,
        "valores": participantes,
        "colores": colores,
        "ejeX": "Eventos",
        "ejeY": "Participantes",
    }
    return data


def obtener_participantes_eventos_todos():
    etiquetas = []
    participantes = []

    participantes_inscritos = ParticipantesEventos.objects.all()
    top_3_eventos = (
        participantes_inscritos.values("evento__nombre")
        .annotate(participantes=Count("evento__nombre"))
        .order_by("-participantes")
    )

    for evento in top_3_eventos:
        etiquetas.append(evento["evento__nombre"])
        participantes.append(evento["participantes"])
    data = {"etiquetas": etiquetas, "valores": participantes}

    return data


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_participantes_evento_datos(request):
    datos_enviar = obtener_eventos_top_participantes()
    return JsonResponse(datos_enviar)


@verificar_modulo_incluido("Reportes")
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
@vigencia_disponible_empresa
def reporte_participantes_evento(request):
    datos_tabla = obtener_participantes_eventos_todos()
    etiquetas = datos_tabla["etiquetas"]
    valores = datos_tabla["valores"]
    data = []
    for iterador in range(len(etiquetas)):
        data.append({"label": etiquetas[iterador], "value": valores[iterador]})

    return render(request, "Reporte/reporte_participantes_evento.html", {"datos": data})


def obtener_asistencias_eventos_top():
    import random

    etiquetas = []
    colores = []
    asistencias = []
    color_aleatorio = lambda: random.randint(0, 255)

    asistencias_participantes = ParticipantesEventos.objects.filter(
        estado_asistencia="ASISTIÓ"
    )
    top_5_asistencias = (
        asistencias_participantes.values("evento__nombre")
        .annotate(asistencias=Count("evento__nombre"))
        .order_by("-asistencias")[:5]
    )

    for evento in top_5_asistencias:
        etiquetas.append(evento["evento__nombre"])
        asistencias.append(evento["asistencias"])
        colores.append(
            "#%02X%02X%02X" % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        )

    data = {
        "etiquetas": etiquetas,
        "valores": asistencias,
        "colores": colores,
        "ejeX": "Eventos",
        "ejeY": "Asistencias",
    }

    return data


def obtener_asistencias_eventos_todos():
    etiquetas = []
    asistencias = []

    asistencias_participantes = ParticipantesEventos.objects.filter(
        estado_asistencia="ASISTIÓ"
    )
    top_5_asistencias = (
        asistencias_participantes.values("evento__nombre")
        .annotate(asistencias=Count("evento__nombre"))
        .order_by("-asistencias")
    )

    for evento in top_5_asistencias:
        etiquetas.append(evento["evento__nombre"])
        asistencias.append(evento["asistencias"])
    data = {"etiquetas": etiquetas, "valores": asistencias}

    return data


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_asistencia_evento_datos(request):
    datos_enviar = obtener_asistencias_eventos_top()
    return JsonResponse(datos_enviar)


@verificar_modulo_incluido("Reportes")
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
@vigencia_disponible_empresa
def reporte_asistencia_evento(request):
    datos_tabla = obtener_asistencias_eventos_todos()
    etiquetas = datos_tabla["etiquetas"]
    valores = datos_tabla["valores"]
    data = []
    for iterador in range(len(etiquetas)):
        data.append({"label": etiquetas[iterador], "value": valores[iterador]})
    return render(request, "Reporte/reporte_asistencia_evento.html", {"datos": data})


def obtener_cantidad_eventos_todos(anho=0):
    print(anho)
    import random

    etiquetas = []
    colores = []
    eventos = []
    mesEvento = []
    anhoEvento = []
    cantidadMes = 0
    meses_nombre = [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ]
    meses_numero = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    color_aleatorio = lambda: random.randint(0, 255)

    eventosConsulta = Evento.objects.all()
    meses_eventos = eventosConsulta.values("fecha_inicio")

    for evento in meses_eventos:
        mesEvento.append(evento["fecha_inicio"].month)
        anhoEvento.append(evento["fecha_inicio"].year)

    if int(anho) == 0:
        for mes in meses_numero:
            for mesDelEvento in mesEvento:
                if mesDelEvento == mes:
                    cantidadMes = cantidadMes + 1

            etiquetas.append(mes)
            eventos.append(cantidadMes)
            colores.append(
                "#%02X%02X%02X"
                % (color_aleatorio(), color_aleatorio(), color_aleatorio())
            )
            cantidadMes = 0
    else:
        for mes in meses_numero:
            for iterador in range(len(mesEvento)):
                if mesEvento[iterador] == mes and anhoEvento[iterador] == int(anho):
                    cantidadMes = cantidadMes + 1

            etiquetas.append(mes)
            eventos.append(cantidadMes)
            colores.append(
                "#%02X%02X%02X"
                % (color_aleatorio(), color_aleatorio(), color_aleatorio())
            )
            cantidadMes = 0

    data = {
        "etiquetas": meses_nombre,
        "valores": eventos,
        "colores": colores,
        "ejeX": "Mes",
        "ejeY": "Cantidad de Eventos",
    }
    return data


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_cantidad_evento_datos(request):
    datos_enviar = obtener_cantidad_eventos_todos(request.GET["anho"])
    return JsonResponse(datos_enviar)


@verificar_modulo_incluido("Reportes")
@vigencia_disponible_empresa
@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def reporte_cantidad_evento(request):
    datos_tabla = obtener_cantidad_eventos_todos()
    etiquetas = datos_tabla["etiquetas"]
    valores = datos_tabla["valores"]
    data = []
    for iterador in range(len(etiquetas)):
        data.append({"label": etiquetas[iterador], "value": valores[iterador]})

    form = filtroAnoForm()
    return render(
        request,
        "Reporte/reporte_eventos_por_mes_evento.html",
        {"datos": data, "form": form},
    )


def obtener_porcentaje_participacion_fecha(fecha_inicio, fecha_fin, separacion="dia"):
    """
    a partir de 2 fechas calculamos el valor participantes/cupos de los eventos que tuvieron lugar entre esas dos fechas
    separar las fechas que se van a mostrar

    fecha = fecha_inicio
    while fecha < fecha_fin:
        fechas.add(fecha)
        fecha.day += 1

    1. obtener los eventos cuya fecha de inicio y fecha de fin se encuentra entre el rengo de fechas

    Eventos.objects.filter(fecha_inicio__in=[inicio, fin]....)


    2. sumar por dia , mes, o año los participantes de los eventos que completaron al inscripcion y los cupos

    for fecha in fechas:
        cupos = 0
        participantes= 0
        for evento in eventos
           if  evento . fecha_inicio <= fecha <= evento.fecha_fin:
                cupos += evento.cupos
                participantes += Participante.objects.filter(evento=evento,estado="Inscrito").count()


    3. obtener el porcentaje de asistencia a partir de eso dos datos


    :param fecha_inicio:
    :param fecha_fin:
    :param separacion = ""
    :return:
    """
    import random

    color_aleatorio = lambda: random.randint(0, 255)

    fechas = []
    conteo = []
    colores = []
    datos_tabla = []
    fechas_formateadas = []
    if type(fecha_inicio) == str or type(fecha_fin) == str:
        fecha_inicio = datetime.datetime.strptime(fecha_inicio, "%Y-%m-%d")
        fecha_fin = datetime.datetime.strptime(fecha_fin, "%Y-%m-%d")

    fecha_inicio_while = fecha_inicio
    while fecha_inicio_while < fecha_fin:
        fechas.append(fecha_inicio_while)
        fecha_inicio_while += datetime.timedelta(days=1)

    eventos_rango_fechas = Evento.objects.annotate(
        inicio=TruncDate("fecha_inicio"), fin=TruncDate("fecha_fin")
    ).filter(
        inicio__range=(fecha_inicio, fecha_fin), fin__range=(fecha_inicio, fecha_fin)
    )
    print(eventos_rango_fechas)
    for fecha in fechas:
        cupos = 0
        asistentes = 0
        for evento in eventos_rango_fechas:
            if evento.fecha_inicio <= fecha.date() <= evento.fecha_fin:
                cupos += evento.numero_participantes
                asistentes += evento.participantes_evento.filter(
                    estado_asistencia="ASISTIÓ"
                ).count()

        porcentaje = (asistentes / cupos) * 100.0 if cupos > 0 else 0.0
        porcentaje = float("%.2f" % porcentaje)
        conteo.append(porcentaje)
        colores.append(
            "#%02X%02X%02X" % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        )

        fecha_formateada = fecha.strftime("%d, %b %Y")
        datos_tabla.append({"0": fecha_formateada, "1": "%.2f%s" % (porcentaje, "%")})
        fechas_formateadas.append(fecha_formateada)

    data = {
        "etiquetas": fechas_formateadas,
        "valores": conteo,
        "colores": colores,
        "datos_tabla": datos_tabla,
        "ejeX": "Fechas",
        "ejeY": "Porcentaje de asistencia",
    }

    return data


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def porcentaje_participacion_fecha_datos(request):
    datos_enviar = obtener_porcentaje_participacion_fecha(
        request.GET["fecha_inicio"], request.GET["fecha_fin"]
    )

    return JsonResponse(datos_enviar)


@verificar_cargo(cargos_permitidos=["Operador", "Gerente"])
def porcentaje_participacion_fecha(request):
    encabezados = ["Fecha", "Porcentaje de asistencia"]

    return render(
        request,
        "Reporte/reporte_porcentaje_asistencia_fecha.html",
        {"encabezados": encabezados},
    )
