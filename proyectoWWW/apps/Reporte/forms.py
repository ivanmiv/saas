from django import forms
from django_select2.forms import Select2Widget
from apps.Evento.models import Evento


class filtroAnoForm(forms.Form):
    filtro_ano = forms.ChoiceField(
        choices=(lambda: Evento.generar_anos()),
        widget=Select2Widget(),
        label="Seleccione un año",
    )
