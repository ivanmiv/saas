from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib import messages

from proyectoWWW.utilidades import verificar_cargo, enviar_email

from .forms import *


@verificar_cargo(cargos_permitidos=["Administrador"])
def registrar_empresa(request):
    """
    Función que permite registrar un tenant (empresa) en el sistema
    :param request:
    :return:
    """
    if request.method == "POST":
        form = EmpresaForm(request.POST)
        if form.is_valid():
            from django.db import connection

            empresa = form.save()
            dominio = Dominio(
                domain=("%s%s") % (empresa.schema_name, settings.DOMINIO),
                is_primary=True,
                tenant=empresa,
            )
            dominio.save()
            connection.set_tenant(empresa)
            Usuario.crear_super_admin_cliente(empresa.cliente)
            connection.set_schema_to_public()
            messages.success(request, "La empresa ha sido registrada exitosamente.")
            return redirect("empresa_registrar_empresa")
        else:
            messages.error(
                request,
                "No se pudo registrar la empresa, por favor verificar los datos.",
            )
    else:
        form = EmpresaForm()
    return render(
        request,
        "empresa/empresa_gestion.html",
        {
            "form": form,
            "titulo": "Registro de empresas",
            "dominios": Dominio.objects.exclude(tenant__schema_name="public")
            .select_related("tenant")
            .all(),
            "dominio": settings.DOMINIO,
        },
    )


@verificar_cargo(cargos_permitidos=["Administrador"])
def modificar_empresa(request, empresa_id):
    """
    Función que permite modificar el nombre de empresa del tenant
    :param request:
    :param empresa_id:
    :return:
    """
    empresa = Empresa.buscar(empresa_id)
    if not empresa:
        messages.error(request, "No existe la empresa solicitada.")
        return redirect("empresa_registrar_empresa")
    if empresa.schema_name == "public":
        return redirect("empresa_registrar_empresa")

    if request.method == "POST":
        form = ModificarEmpresaForm(request.POST, instance=empresa)
        if form.is_valid():
            form.save()
            messages.success(request, "La empresa ha sido modificada exitosamente.")
            return redirect("empresa_registrar_empresa")
        else:
            messages.error(
                request,
                "No se pudo modificar la empresa, por favor verificar los datos.",
            )
    else:
        form = ModificarEmpresaForm(instance=empresa)

    return render(
        request,
        "empresa/empresa_gestion.html",
        {
            "form": form,
            "titulo": "Modificación de empresa",
            "dominios": Dominio.objects.exclude(tenant__schema_name="public")
            .select_related("tenant")
            .all(),
            "dominio": settings.DOMINIO,
        },
    )


@verificar_cargo(cargos_permitidos=["Cliente"])
def solicitar_registro_empresa(request, plan_id=1):
    """
        Función que permite registrar una solicitud de creación de tenant (empresa) en el sistema
        :param request:
        :param plan_id: Identificador del plan
        :return:
    """
    plan = Plan.buscar(plan_id)
    if not plan:
        messages.error(request, "No existe el plan buscado")
        return redirect("inicio")

    if request.method == "POST":
        form = SolicitudEmpresaForm(request.POST)
        if form.is_valid():
            solicitud = form.save(commit=False)
            solicitud.solicitante = request.user
            solicitud.plan = plan
            solicitud.save()
            messages.success(request, "La solicitud ha sido registrada exitosamente.")
            return redirect("inicio")
        else:
            messages.error(
                request,
                "No se pudo registrar la solicitud, por favor verificar los datos.",
            )
    else:
        form = SolicitudEmpresaForm()
    return render(
        request,
        "empresa/solicitud_registro_empresa.html",
        {"form": form, "dominio": settings.DOMINIO},
    )


@verificar_cargo(cargos_permitidos=["Administrador"])
def listado_solicitudes(request):
    """
        Función que lista las solicitudes que no hayan recibido respuesta, ya sea aceptada o rechazada
        :param request:
        :return:
    """
    solicitudes = SolicitudEmpresa.objects.filter(aceptada=None)
    return render(
        request, "empresa/solicitud_listado.html", {"solicitudes": solicitudes}
    )


def aceptar_rechazar_solicitud(request, id_solicitud, estado_aprobacion):
    """
    Función que permite aceptar o rechazar una solicitud y crear una empresa a partir de ella

    :param request:
    :param id_solicitud:
    :param estado_aprobacion:
    :return:
    """

    try:
        solicitud = SolicitudEmpresa.objects.get(id=id_solicitud)
    except Exception:
        messages.error(request, "No existe la solicitud")
        return redirect("empresa_listado_solicitudes")

    if solicitud.aceptada != None:
        messages.error(request, "No es posible modificar la solicitud")
        return redirect("empresa_listado_solicitudes")

    if estado_aprobacion == "0":
        solicitud.aceptada = False
        messages.success(request, "Solicitud rechazada")
        solicitud.save()
    elif estado_aprobacion == "1":
        form = RegistrarEmpresaSolicitudForm(
            {
                "nombre": solicitud.nombre,
                "schema_name": solicitud.schema_name,
                "tema": solicitud.tema,
                "color": solicitud.color,
                "vigencia": solicitud.vigencia,
            }
        )
        if form.is_valid():
            from django.db import connection

            empresa = form.save(commit=False)
            empresa.cliente = solicitud.solicitante
            empresa.plan = solicitud.plan
            empresa.save()
            dominio = Dominio(
                domain=("%s%s") % (empresa.schema_name, settings.DOMINIO),
                is_primary=True,
                tenant=empresa,
            )
            dominio.save()
            connection.set_tenant(empresa)
            Usuario.crear_super_admin_cliente(empresa.cliente)

            cliente = empresa.cliente
            datos_email = {
                "subject": "Solicitud de empresa aceptada",
                "body": "Su usuario es: admin, su contraseña es: %s-%s, su dominio es: %s%s"
                % (cliente.cedula, cliente.first_name, empresa.schema_name, settings.DOMINIO),
                "to": [cliente.email],
                "mensaje_error": "Error al enviar correo",
            }
            enviar_email(request=None, **datos_email)
            solicitud.aceptada = True
            messages.success(request, "Solicitud aceptada")
            solicitud.save()
        else:
            solicitud.aceptada = None
            print(form.errors)
            messages.error(
                request,
                "No se ha podido aceptar la solicitud, por favor verificar los datos.",
            )
            solicitud.save()

    return redirect("empresa_listado_solicitudes")


@verificar_cargo(cargos_permitidos=["Cliente"])
def gestionar_empresas(request, empresa_id=None):
    """
    Función que permite gestionar sus emrepesas a un cliente
    :param request:
    :param empresa_id:
    :return:
    """
    form = None
    empresa = Empresa.buscar(empresa_id)
    if empresa:
        form = ModificarEmpresaClienteForm(instance=empresa)

    if request.method == "POST":
        form = ModificarEmpresaClienteForm(request.POST, instance=empresa)
        if form.is_valid():
            form.save()
            messages.success(request, "La empresa ha sido modificada exitosamente.")
            return redirect("empresa_gestionar_empresas_cliente")
        else:
            messages.error(
                request,
                "No se pudo modificar la empresa, por favor verificar los datos.",
            )

    return render(
        request,
        "empresa/empresa_gestion_cliente.html",
        {
            "form": form,
            "titulo": "Gestión de empresa",
            "dominios": Dominio.objects.filter(tenant__cliente=request.user)
            .select_related("tenant")
            .all(),
            "dominio": settings.DOMINIO,
        },
    )


@verificar_cargo(cargos_permitidos=["Cliente"])
def obtener_copia_seguridad(request, empresa_id):
    """
    Función que permite obtener un backup de los datos de la empresa a un cliente
    :param request:
    :param empresa_id:
    :return:
    """
    import sys
    from io import StringIO

    from django.http import HttpResponse
    from django.core.management import call_command
    from django.db import connection

    from apps.empresa.models import Empresa

    backup_empresa = StringIO()

    try:
        empresa = Empresa.buscar(empresa_id)
        if empresa:
            connection.set_tenant(empresa)
            sysout = sys.stdout
            sys.stdout = backup_empresa
            call_command(
                "dumpdata", indent=4, exclude=["auth", "sessions", "contenttypes"]
            )
            sys.stdout = sysout
            connection.set_schema_to_public()
        else:
            messages.error(
                request, "Error al generar el archivo, por favor intente nuevamente."
            )
            return redirect("empresa_gestionar_empresas_cliente")

    except Exception:
        messages.error(
            request, "Error al generar el archivo, por favor intente nuevamente."
        )
        return redirect("empresa_gestionar_empresas_cliente")

    backup_empresa.seek(0)
    response = HttpResponse(backup_empresa, content_type="application/text")
    backup_empresa.close()
    response["Content-Disposition"] = (
        "attachment; filename=backup-%s.json" % empresa.nombre
    )

    return response


@verificar_cargo(cargos_permitidos=["Administrador"])
def editar_planes(request):

    from django.forms.models import modelformset_factory

    planes = Plan.objects.all()
    total_planes = len(planes)

    planesFormSet = modelformset_factory(
        Plan, form=PlanForm, extra=total_planes, max_num=total_planes
    )
    formset_planes = planesFormSet(queryset=planes)

    if request.method == "POST":
        formset_planes = planesFormSet(request.POST, request.FILES, queryset=planes)
        if formset_planes.is_valid():
            for form in formset_planes.forms:
                form.save()

            messages.success(
                request,
                "La configuración de los planes ha sido modificada satisfactoriamente",
            )
            return redirect("empresa_editar_planes_empresa")
        else:
            for form_educacion in formset_planes.forms:
                print(form_educacion.is_valid())
            messages.error(
                request,
                "La configuración de los planes no ha sido modificada satisfactoriamente, por favor inténtelo de nuevo",
            )

    return render(
        request, "empresa/gestion_planes.html", {"formset_planes": formset_planes}
    )
