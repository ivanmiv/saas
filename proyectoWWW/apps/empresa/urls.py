from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r"^registrar$", registrar_empresa, name="empresa_registrar_empresa"),
    url(
        r"^modificar/(?P<empresa_id>\d+)$",
        modificar_empresa,
        name="empresa_modificar_empresa",
    ),
    url(
        r"^registrar-solicitud$",
        solicitar_registro_empresa,
        name="empresa_registrar_solicitud_empresa",
    ),
    url(
        r"^registrar-solicitud/(?P<plan_id>\d+)$",
        solicitar_registro_empresa,
        name="empresa_registrar_solicitud_empresa",
    ),
    url(
        r"^listado-solicitudes$",
        listado_solicitudes,
        name="empresa_listado_solicitudes",
    ),
    url(r"^gestionar$", gestionar_empresas, name="empresa_gestionar_empresas_cliente"),
    url(
        r"^gestionar/modificar/(?P<empresa_id>\d+)$",
        gestionar_empresas,
        name="empresa_modificar_empresa_cliente",
    ),
    url(
        r"^copia-seguridad/(?P<empresa_id>\d+)$",
        obtener_copia_seguridad,
        name="empresa_copia_seguridad_empresa",
    ),
    url(
        r"^aceptar-rechazar-solicitud/(?P<id_solicitud>\d+)/(?P<estado_aprobacion>\d+)$",
        aceptar_rechazar_solicitud,
        name="empresa_aceptar_rechazar_solicitud",
    ),
    url(r"^editar-planes$", editar_planes, name="empresa_editar_planes_empresa"),
]
