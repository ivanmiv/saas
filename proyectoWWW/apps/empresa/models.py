from django.core.exceptions import ValidationError
from django.db import models
from django_tenants.models import TenantMixin, DomainMixin

from simple_history.models import HistoricalRecords
from django_tenants.postgresql_backend.base import _is_valid_schema_name

from apps.Usuario.models import Usuario

CHOICES_TEMA = (
    ("a", "Tema A"),
    ("b", "Tema B"),
    ("c", "Tema C"),
    ("d", "Tema D"),
    ("e", "Tema E"),
)

CHOICES_COLOR = (
    ("coffee", "Café"),
    ("dark", "Oscuro"),
    ("dust", "Arena"),
    ("gray", "Gris"),
    ("lime", "Lima"),
    ("mint", "Menta"),
    ("navy", "Azul marino"),
    ("ocean", "Oceano"),
    ("prickly-pear", "Higo"),
    ("purple", "Violeta"),
    ("well-red", "Rojo"),
    ("yellow", "Amarillo"),
)


class Modulo(models.Model):
    nombre = models.CharField(max_length=100)

    @staticmethod
    def crear_modulos_iniciales():
        if Modulo.objects.all().count() == 0:
            Modulo.objects.create(nombre="Actividades")
            Modulo.objects.create(nombre="Calendario")
            Modulo.objects.create(nombre="Eventos")
            Modulo.objects.create(nombre="Noticias")
            Modulo.objects.create(nombre="Participantes")
            Modulo.objects.create(nombre="Reportes")
            Modulo.objects.create(nombre="Temas")
            Modulo.objects.create(nombre="Usuarios")

    def __str__(self):
        return self.nombre


class Plan(models.Model):
    nombre = models.CharField(max_length=100)
    precio = models.PositiveIntegerField()
    historia = HistoricalRecords()
    modulos_disponibles = models.ManyToManyField(Modulo)
    maximo_usuarios = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre

    @staticmethod
    def buscar(id):
        try:
            return Plan.objects.get(id=id)
        except Plan.DoesNotExist:
            return None

    @staticmethod
    def crear_planes_iniciales():
        Modulo.crear_modulos_iniciales()
        if Plan.objects.all().count() == 0:
            plan = Plan.objects.create(nombre="Gratuito", precio=0, maximo_usuarios=5)
            plan.modulos_disponibles = Modulo.objects.filter(
                nombre__in=[
                    "Actividades",
                    "Calendario",
                    "Eventos",
                    "Noticias",
                    "Participantes",
                    "Temas",
                    "Usuarios",
                ]
            )
            plan.save()
            plan = Plan.objects.create(
                nombre="Estandar", precio=25, maximo_usuarios=100
            )
            plan.modulos_disponibles = Modulo.objects.filter(
                nombre__in=[
                    "Actividades",
                    "Calendario",
                    "Eventos",
                    "Noticias",
                    "Participantes",
                    "Reportes",
                    "Temas",
                    "Usuarios",
                ]
            )
            plan.save()
            plan = Plan.objects.create(
                nombre="Empresarial", precio=199, maximo_usuarios=1000
            )
            plan.modulos_disponibles = Modulo.objects.filter(
                nombre__in=[
                    "Actividades",
                    "Calendario",
                    "Eventos",
                    "Noticias",
                    "Participantes",
                    "Reportes",
                    "Temas",
                    "Usuarios",
                ]
            )
            plan.save()

    def obtener_modulos_disponibles(self):
        return [str(modulo) for modulo in self.modulos_disponibles.all()]


def check_schema_name(name):
    if not _is_valid_schema_name(name):
        raise ValidationError("El nombre del esquema es inválido.")


class Empresa(TenantMixin):
    nombre = models.CharField(
        max_length=300, verbose_name="nombre de la empresa", unique=True
    )
    schema_name = models.CharField(
        max_length=63, unique=True, validators=[check_schema_name]
    )
    historia = HistoricalRecords()

    plan = models.ForeignKey(Plan, related_name="empresas")
    tema = models.CharField(max_length=100, choices=CHOICES_TEMA, default="a")
    color = models.CharField(max_length=100, choices=CHOICES_COLOR, default="mint")
    cliente = models.ForeignKey(
        Usuario, verbose_name="cliente de la empresa", related_name="mi_empresa"
    )
    vigencia = models.DateTimeField(
        help_text="Fecha hasta la que se encuentra pago el servicio"
    )

    historia = HistoricalRecords()

    def __str__(self):
        return self.nombre

    @staticmethod
    def buscar(id):
        try:
            return Empresa.objects.get(id=id)
        except Empresa.DoesNotExist:
            return None


class Dominio(DomainMixin):
    historia = HistoricalRecords()


class SolicitudEmpresa(models.Model):
    solicitante = models.ForeignKey(Usuario, related_name="solicitudes")
    nombre = models.CharField(
        max_length=300, unique=True, verbose_name="nombre de la empresa"
    )
    schema_name = models.CharField(
        max_length=63, unique=True, validators=[check_schema_name]
    )
    aceptada = models.NullBooleanField()

    tema = models.CharField(max_length=100, choices=CHOICES_TEMA, default="a")
    color = models.CharField(max_length=100, choices=CHOICES_COLOR, default="mint")
    plan = models.ForeignKey(Plan, verbose_name="plan", related_name="solicitud")
    vigencia = models.DateTimeField(
        help_text="Fecha hasta la que se encuentra pago el servicio"
    )

    historia = HistoricalRecords()
