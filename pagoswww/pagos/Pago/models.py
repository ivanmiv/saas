from django.db import models


class Pago(models.Model):
    cedula = models.PositiveIntegerField(
        verbose_name="Número de documento de identificación"
    )
    identificador_evento = models.PositiveIntegerField(
        verbose_name="Identificador del evento a pagar"
    )
    identificador_empresa = models.CharField(
        max_length=150, verbose_name="Nombre de la empresa"
    )
    esta_pago = models.BooleanField(verbose_name="¿Pago realizado?")

    class Meta:
        unique_together = ("cedula", "identificador_evento")
