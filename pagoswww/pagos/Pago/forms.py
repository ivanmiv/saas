from django import forms
from django.core.exceptions import ObjectDoesNotExist

from .models import *


class PagoForm(forms.ModelForm):
    class Meta:
        model = Pago
        exclude = ("esta_pago",)

    def clean(self):
        form_data = self.cleaned_data
        try:
            pago = Pago.objects.get(
                cedula=form_data["cedula"],
                identificador_evento=form_data["identificador_evento"],
            )
            self._errors["cedula"] = [
                "La cédula ya esta asociada con el identificador de evento %s"
                % form_data["identificador_evento"]
            ]
        except ObjectDoesNotExist:
            pass
        return form_data
